# React Packages

---

This repository is a [monorepo](http://www.drmaciver.com/2016/10/why-you-should-use-a-single-repository-for-all-your-companys-projects/) intended to host reusable React components and containers thanks to [Lerna](https://github.com/lerna/lerna) and [Yarn Workspaces](https://yarnpkg.com/blog/2017/08/02/introducing-workspaces/).

A live storybook is available to see how the components looks like @ http://react-packages.ws3.4ge.it

You can find all the React components and containers under the `packages` folder.

## Installation

1. This monorepo requires `yarn` version `>=1.0.0` to work properly. If you have a previous version you can use the following command line to get it upgraded:

```bash
sudo npm i -g yarn
```

2. A recommended way to install ***@4geit/react-packages*** is through [git](//gitlab.com/4geit/react-packages) repository using the following command:

```bash
git clone git@gitlab.com:4geit/react-packages.git
```

alternatively you can use the HTTPS URL:

```bash
git clone https://gitlab.com/4geit/react-packages.git
```

And finally change your directory to the folder `react-packages`:

```bash
cd react-packages
```

3. Now you are ready to install all the packages dependencies by using simply the command line:

```bash
yarn
```

4. (Optional), you can also skip typing the author name and email each time that you want to create a package/project by adding those lines in your `~/.bashrc` file:

```bash
export AUTHOR_NAME="John Doe"
export AUTHOR_EMAIL="john.doe@provider.xyz"
```

* `<AUTHOR_NAME>`: the name of the author stored in the `package.json` generated when creating a new package/project.
* `<AUTHOR_EMAIL>`: the name of the author stored in the `package.json` generated when creating a new package/project.

## Development

### Storybook

You can see a live preview of those packages by running the storybook server with the following command line:

```bash
yarn storybook
```

And the instance will be running at http://localhost:6006

This command also enables the watch mode therefore any changes that happens in a package will apply the changes in the storybook interface.

### Watch mode alone

If you want to run the watch mode alone in order to build the package automatically while you are making changes to them, you can use the following command:

```bash
yarn watch
```

Caution: Don't use this command if you are already using `yarn storybook`

## Package generator

### Create a new package

1. The easiest way to generate a new package with `react-packages` is to use the following command line:

```bash
yarn create-new-package
```

The command will ask a set of values such as:

* `<NAME>`: the name of the repository the one used in `rct-<NAME>-<TYPE>`. It has to comply with the [slug](https://blog.tersmitten.nl/slugify/) format.
* `<TYPE>`: the type of the repository the one used in `rct-<NAME>-<TYPE>` it can contain the value `component` or `store`.
* `<DESCRIPTION>`: the description of the project stored in the `package.json` and `README.md`

2. But you can also use the command argument by doing so:

```bash
yarn create-new-package <NAME> <TYPE>
```

You will still be asked to type the description though.

3. Once the package is generated, you will see it available under the folder `./packages`.

4. You are now ready to use your new package by reading its `README.md` file.

### Create a new project

1. You will need to add those two additional environment variables in your `~/.bashrc` file:

```bash
export GITLAB_PRIVATE_TOKEN=YOUR_GITLAB_PRIVATE_TOKEN
export RUNNER_ID=YOUR_RUNNER_ID
```

* `GITLAB_PRIVATE_TOKEN`: a gitlab token allowed to create a project under the group `4geit`. You can get your private gitlab token [right here](https://gitlab.com/profile/account) under the `Private token` section.
* `RUNNER_ID`: the runner id that will run the CI build. Ask your team in order to provide you this ID.

Plus, you will also need to install the tool `jq` if it's not installed yet, assuming you are using `Debian` and `Ubuntu`, you can use the following command line:

```bash
sudo apt install jq
```

2. The easiest way to generate a new project with `react-packages` is to use the following command line:

```bash
yarn create-new-project
```

* `<GROUP>`: the group repository where it will be located on gitlab.com, the one used in `<GROUP>/<NAME>`
* `<NAME>`: the name of the repository the one used in `<GROUP>/<NAME>`
* `<DESCRIPTION>`: the description of the project stored in the `package.json` and `README.md`

3. But you can also use the command argument by doing so:

```bash
yarn create-new-project <GROUP> <NAME>
```

You will still be asked to type the description though.

4. You will need to run the `yarn` command in order to install its dependencies.

## Storybook

The storybook interface allows to test quickly the react components available in the repository. Each component is added as a story.

Let's assume you create a new component package named `RctDummyComponent` with the package name `@4geit/rct-dummy-component` and you are willing to see it appears in the storybook interface. Therefore you will need to edit the file `stories/index.js`, import the component class `RctDummyComponent` as follows:

```js
import RctDummyComponent from '@4geit/rct-dummy-component'
```

and then at the bottom of the file add the following code:

```js
storiesOf('RctDummyComponent', module)
  .addDecorator(centered)
  .add('simple usage',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <RctDummyComponent/>
    ))
  )
```

as you might have noticed the string `RctDummyComponent` is used to pass a named to the story, `.addDecorator(centered)` is used to center the component UI to the center of the screen and `.add('simple usage', ...)` is used to add one example of the story with a given designation. Here we are calling the component directive `<RctDummyComponent/>`.

And you can then run the storybook instance with the command:

```bash
yarn storybook
```

and get access to it through http://localhost:6006
