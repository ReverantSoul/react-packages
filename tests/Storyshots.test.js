import initStoryshots from '@storybook/addon-storyshots'
initStoryshots()

// fix https://github.com/callemall/material-ui/issues/5354#issuecomment-274020532
jest.mock('material-ui/internal/EnhancedSwitch')
jest.mock('material-ui/internal/Tooltip')
