// eslint-disable-next-line
import { observable, action, runInAction, toJS } from 'mobx'
// eslint-disable-next-line
import { hashHistory } from 'react-router'

class <CLASS> {
  // @observable var1
  // @observable var2
  // @action setVar1(value) {
  //   this.var1 = value
  // }
  // @action setVar2(value) {
  //   this.var2 = value
  // }
  // @computed get dynamicVar1() {
  //   return this.var1.filter(i => ...)
  // }
}

export default new <CLASS>()
