import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
// eslint-disable-next-line
import classNames from 'classnames'

import './rct-<NAME>.<TYPE>.css'

@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  // TBD
})
@defaultProps({
  // TBD
})
export default class <CLASS> extends Component {
  render() {
    // const { ... } = this.props
    return (
      <p>
        <CLASS> works!
      </p>
    )
  }
}
