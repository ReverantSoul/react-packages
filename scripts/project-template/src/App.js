import React, { Component } from 'react'
// eslint-disable-next-line
import { Switch, Route, withRouter, Link } from 'react-router-dom'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'

function PrivateRoute({ component: Component, authed, ...rest }) {
  return (
    <Route
      { ...rest }
      render={ props => authed === true
        ? <Component { ...props } />
        : <Redirect to={ { pathname: '/login', state: { from: props.location } } } /> }
    />
  )
}

// @inject('swaggerClientStore', 'commonStore')
@withRouter
@observer
class App extends Component {
  // async componentWillMount() {
  //   await this.props.swaggerClientStore.buildClient()
  //   this.props.commonStore.setAppLoaded()
  // }

  render() {
    // const { isLoggedIn, appLoaded } = this.props.commonStore
    // if (!appLoaded) {
    //   return ( <div>Loading...</div> )
    // }
    return (
      <div>
        {/*
        <Switch>
          <Route path="/aaa" component={ AaaComponent } />
          <Route path="/bbb" component={ BbbComponent } />
          <Route path="/ccc" component={ CccComponent } />
          <PrivateRoute authed={ isLoggedIn } path="/ddd" component={ DddComponent } />
          <Route path="/" component={ HomeComponent } />
        </Switch>
        */}
      </div>
    )
  }
}

export default App
