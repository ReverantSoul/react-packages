import React from 'react'
import { render } from 'react-dom'
import { useStrict } from 'mobx'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'mobx-react'
// load roboto font to entrypoint
import 'typeface-roboto'
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import blue from 'material-ui/colors/blue'
import pink from 'material-ui/colors/pink'

import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink,
  },
})

const stores = {
}

// For easier debugging
window._____APP_STATE_____ = stores

useStrict(true)

render((
  <Provider { ...stores }>
    <BrowserRouter>
      {/* temporally disabled because of an issue with breakpoints API */}
      {/* <MuiThemeProvider theme={ theme } > */}
        <App />
      {/* </MuiThemeProvider> */}
    </BrowserRouter>
  </Provider>
), document.getElementById('root'))

registerServiceWorker()
