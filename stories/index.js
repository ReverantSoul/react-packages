import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
import { observable, computed, action, useStrict } from 'mobx'
import { Provider, inject, observer } from 'mobx-react'
import Swagger from 'swagger-client'
import { BrowserRouter, Switch, Route, withRouter, Link } from 'react-router-dom'

import { storiesOf, addDecorator } from '@storybook/react'
import { action as storybookAction } from '@storybook/addon-actions'
import { withInfo } from '@storybook/addon-info'
import { linkTo } from '@storybook/addon-links'
import centered from '@storybook/addon-centered'

import RctLoginComponent from '@4geit/rct-login-component'
import RctRegisterComponent from '@4geit/rct-register-component'
import RctDataTableComponent from '@4geit/rct-data-table-component'
import RctDummyComponent from '@4geit/rct-dummy-component'
import RctLayoutComponent from '@4geit/rct-layout-component'
import RctHeaderComponent from '@4geit/rct-header-component'
import RctSearchInputComponent from '@4geit/rct-search-input-component'
import RctProjectComponent from '@4geit/rct-project-component'
import RctSideMenuComponent, { SideMenuItem } from '@4geit/rct-side-menu-component'
import RctFooterComponent, { FooterItem } from '@4geit/rct-footer-component'
import RctNotificationComponent from '@4geit/rct-notification-component'
import RctRightSideMenuComponent from '@4geit/rct-right-side-menu-component'
import RctChatboxListComponent from '@4geit/rct-chatbox-list-component'
import RctChatboxGridComponent from '@4geit/rct-chatbox-grid-component'
import RctBroadcastMessageComponent from '@4geit/rct-broadcast-message-component'
import RctEventListComponent from '@4geit/rct-event-list-component'
import RctDatePickerComponent from '@4geit/rct-date-picker-component'
import RctReorderableGridListComponent, { ReorderableGridListTile } from '@4geit/rct-reorderable-grid-list-component'
import RctNotificationMenuComponent from '@4geit/rct-notification-menu-component'
import RctAccountComponent from '@4geit/rct-account-component'


import commonStore from '@4geit/rct-common-store'
import authStore from '@4geit/rct-auth-store'
import swaggerClientStore from '@4geit/rct-swagger-client-store'
import dataTableStore from '@4geit/rct-data-table-store'
import notificationStore from '@4geit/rct-notification-store'
import chatboxListStore from '@4geit/rct-chatbox-list-store'
import eventListStore from '@4geit/rct-event-list-store'
import chatboxGridStore from '@4geit/rct-chatbox-grid-store'
import datePickerStore from '@4geit/rct-date-picker-store'
import broadcastMessageStore from '@4geit/rct-broadcast-message-store'
import notificationMenuStore from '@4geit/rct-notification-menu-store'
import accountStore from '@4geit/rct-account-store'

import Card, { CardActions, CardContent } from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import CloseIcon from 'material-ui-icons/Close'
import NotificationsIcon from 'material-ui-icons/Notifications'
import TextField from 'material-ui/TextField'
import Grid from 'material-ui/Grid'
import Paper from 'material-ui/Paper'
import Checkbox from 'material-ui/Checkbox'
import { FormGroup, FormControlLabel } from 'material-ui/Form'
import Typography from 'material-ui/Typography'
import Toolbar from 'material-ui/Toolbar'
import List, {
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
} from 'material-ui/List'
import Divider from 'material-ui/Divider'
import { GridList, GridListTile } from 'material-ui/GridList'
import Menu, { MenuItem } from 'material-ui/Menu'
import Table, { TableBody, TableCell, TableFooter, TableHead, TablePagination, TableRow, TableSortLabel } from 'material-ui/Table'

// load roboto font to entrypoint
import 'typeface-roboto'

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles'
import blue from 'material-ui/colors/blue'
import pink from 'material-ui/colors/pink'

import Logo from './assets/logo.png'

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: pink,
 }
})
const muiThemeDecorator = (story) => (
  <MuiThemeProvider theme={ theme }>
    { story() }
  </MuiThemeProvider>
)
addDecorator(muiThemeDecorator)

class ReorderableGridListStore {
  @observable data = [
    { id: 1, position: 1, backgroundColor: 'black', },
    { id: 2, position: 0, backgroundColor: 'yellow', },
    { id: 3, position: 2, backgroundColor: 'green', },
  ]
  @computed get sortedData() {
    return this.data.slice().sort((a, b) => a.position > b.position)
  }

  @action async fetchData() {
    console.log('fetchData')
  }
  @action async setPosition(itemId, position) {
    const [item] = this.data.filter(({ id }) => id === itemId)
    item.position = position
  }
}
const reorderableGridListStore = new ReorderableGridListStore()

const stores = {
  commonStore,
  authStore,
  swaggerClientStore,
  dataTableStore,
  notificationStore,
  chatboxListStore,
  eventListStore,
  chatboxGridStore,
  datePickerStore,
  broadcastMessageStore,
  reorderableGridListStore,
  notificationMenuStore,
  accountStore
}

// for easier debugging
window.____APP_STATE____ = stores

useStrict(true)

const style = {
  height: 140,
  width: '100%'
}

const HomeComponent = () => (
  <Typography type="title">Home</Typography>
)

@inject('swaggerClientStore', 'commonStore')
@observer
class App extends Component {
  async componentWillMount() {
    const { swaggerClientStore, commonStore } = this.props
    await swaggerClientStore.buildClient({
      apiUrl: process.env.STORYBOOK_API_URL || 'http://localhost:10010/v1'
    })
    await swaggerClientStore.buildClientWithToken({
      token: process.env.STORYBOOK_TOKEN
    })
    const { body: { token, _id, id, ...fields } } = await swaggerClientStore.client.apis.Account.account()
    commonStore.setToken(token)
    commonStore.setUser({ ...fields })
    commonStore.setAppLoaded()
  }

  render() {
    const { commonStore, children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

@inject('swaggerClientStore', 'commonStore')
@observer
class APIApp extends Component {
  async componentWillMount() {
    const { swaggerClientStore, commonStore } = this.props
    await swaggerClientStore.buildClient({
      apiUrl: process.env.STORYBOOK_API_APP_URL || 'http://localhost:10010/v1',
      swaggerUrl: process.env.STORYBOOK_SWAGGER_URL,
    })
    await swaggerClientStore.buildClientWithAuthorizations({
      authorizations: {
        TimeslotSecurity: process.env.STORYBOOK_API_APP_TOKEN,
      }
    })
    console.log(swaggerClientStore.client)
    commonStore.setAppLoaded()
  }

  render() {
    const { commonStore, children } = this.props
    if (commonStore.appLoaded) {
      return (
        <div>
          { children }
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

const WrapReorderableItemComponent = props => (
  <Typography>
    { props.itemId }
  </Typography>
)

@inject('reorderableGridListStore')
@observer
class WrapReorderableGridListComponent extends Component {
  render() {
    const { reorderableGridListStore } = this.props
    const { fetchData, setPosition, sortedData } = reorderableGridListStore
    return (
      <RctReorderableGridListComponent
        cols={ 3 }
        handleSetPosition={ setPosition.bind(reorderableGridListStore) }
        handleFetchData={ fetchData.bind(reorderableGridListStore) }
        data={ sortedData }
        itemComponent={ <WrapReorderableItemComponent/> }
      />
    )
  }
}

@observer
class WrapTable extends Component {
  @observable data = [
    { id: 1, name: 'John Smith', age: '45', genre: 'Male' },
    { id: 2, name: 'Paul Gram', age: '10', genre: 'Male' },
    { id: 3, name: 'Ken Ken', age: '20', genre: 'Male' },
    { id: 4, name: 'Peter Dupond', age: '90', genre: 'Male' },
  ]
  @observable columnData = ['name', 'age', 'genre']
  @observable selected = []

  @action handleSelectAllClick(event, checked) {
    if (checked) {
      this.selected = this.data.map(({ id }) => id)
      return
    }
    this.selected = []
  }
  @action handleClick(event, id) {
    const selectedIndex = this.selected.indexOf(id)
    let newSelected = []
    if (selectedIndex === -1) { // when index not selected yet, add it
      newSelected = newSelected.concat(this.selected.slice(), id)
    } else if (selectedIndex === 0) { // when index is the first item
      newSelected = newSelected.concat(this.selected.slice(1))
    } else if (selectedIndex === this.selected.length - 1) { // when index is the last item
      newSelected = newSelected.concat(this.selected.slice(0, -1))
    } else if (selectedIndex > 0) { // when index is between
      newSelected = newSelected.concat(
        this.selected.slice(0, selectedIndex),
        this.selected.slice(selectedIndex + 1),
      )
    }
    this.selected = newSelected
  }
  isSelected(id) {
    return !!~this.selected.indexOf(id)
  }

  render() {
    const { ...props } = this.props
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={ this.selected.length > 0 && this.selected.length < this.data.length }
                checked={ this.selected.length === this.data.length }
                onChange={ this.handleSelectAllClick.bind(this) }
              />
            </TableCell>
            { this.columnData.map(column => (
              <TableCell key={ column }>{ column }</TableCell>
            )) }
          </TableRow>
        </TableHead>
        <TableBody>
          { this.data.map(row => {
            const isSelected = this.isSelected(row.id)
            return (
              <TableRow key={ row.id } hover>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={ isSelected }
                    onChange={ event => this.handleClick(event, row.id) }
                  />
                </TableCell>
                { this.columnData.map(column => (
                  <TableCell key={ column }>{ row[column] }</TableCell>
                )) }
              </TableRow>
            )
          }) }
        </TableBody>
      </Table>
    )
  }
}

storiesOf('RctDummyComponent', module)
  .addDecorator(centered)
  .add('simple usage',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <RctDummyComponent/>
    ))
  )
storiesOf('MUI', module)
  // Grid
  .add('Grid',
    withInfo('Test Grid')(() => (
      <Grid container>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
        <Grid item xs={6} lg={3}>
          <Paper style={style} />
        </Grid>
      </Grid>
    ))
  )
  // centered
  .addDecorator(centered)
  // Card
  .add('Card',
    withInfo('Test Card')(() => (
      <Card>
        <CardContent>
          <Typography type="body1" gutterBottom>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </Typography>
        </CardContent>
      </Card>
    ))
  )
  // IconButton
  .add('IconButton',
    withInfo('Test IconButton')(() => (
      <IconButton>
        <CloseIcon/>
      </IconButton>
    ))
  )
  // TextField
  .add('TextField',
    withInfo('Test TextField')(() => (
      <TextField label="Email"/>
    ))
  )
  // Checkbox
  .add('Checkbox',
    withInfo('Test Checkbox')(() => (
      <FormGroup row>
        <FormControlLabel
          control={ <Checkbox/> }
          label="Checkbox Label"
        />
        <FormControlLabel
          control={ <Checkbox/> }
          label="Checkbox Label"
        />
      </FormGroup>
    ))
  )
  // Menu
  .add('Menu', () => (
    <Menu open={ true }>
      <MenuItem component="a" href="http://google.com">
        <Typography>test</Typography>
      </MenuItem>
    </Menu>
  ))
  // Table
  .add('Table', () => (
    <Paper>
      <WrapTable/>
    </Paper>
  ))
storiesOf('RctLoginComponent', module)
  .addDecorator(centered)
  .add('with card width',
    withInfo('This is the basic usage of the component with the card width set')(() => (
      <BrowserRouter>
        <Provider { ...stores }>
          <App>
            <RctLoginComponent cardWidth='400px' />
          </App>
        </Provider>
      </BrowserRouter>
    ))
  )
storiesOf('RctRegisterComponent', module)
  .addDecorator(centered)
  .add('with card width',
    withInfo('This is the basic usage of the component with the card width set')(() => (
      <Provider { ...stores }>
        <BrowserRouter>
          <App>
            <RctRegisterComponent cardWidth='400px' />
          </App>
        </BrowserRouter>
      </Provider>
    ))
  )
storiesOf('RctDataTableComponent', module)
  .addDecorator(centered)
  .add('contactList',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <Provider { ...stores }>
        <App>
          <RctDataTableComponent
            title="Contact List"
            listOperationId="contactList"
            deleteOperationId="contactDelete"
            addOperationId="contactAdd"
            updateOperationId="contactUpdate"
            bulkAddOperationId="contactBulkAdd"
            enabledColumns={ [ 'firstname', 'lastname', 'company' ] }
          />
        </App>
      </Provider>
    ))
  )
  .add('productList',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <Provider { ...stores }>
        <App>
          <RctDataTableComponent
            title="Product List"
            listOperationId="productList"
            deleteOperationId="productDelete"
            addOperationId="productAdd"
            updateOperationId="productUpdate"
            bulkAddOperationId="productBulkAdd"
            enabledColumns={ [ 'bloomberCode', 'name', 'currency' ] }
          />
        </App>
      </Provider>
    ))
  )
storiesOf('RctLayoutComponent', module)
  .add('with header, side-menu and content', () => (
    <Provider { ...stores }>
      <BrowserRouter>
        <RctLayoutComponent
          topComponent={ <RctHeaderComponent logo={ Logo } /> }
          sideMenuComponent={
            <RctSideMenuComponent >
              <SideMenuItem icon="inbox" label="Inbox" route="/" />
              <SideMenuItem icon="star" label="Starred" route="/" />
              <SideMenuItem icon="send" label="Send mail" route="/" />
              <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
              <SideMenuItem icon="email" label="All mail" route="/" />
              <SideMenuItem icon="delete" label="Trash" route="/" />
              <SideMenuItem icon="spam" label="Spam" route="/" />
            </RctSideMenuComponent>
          }
          rightSideMenuComponent={
            <RctRightSideMenuComponent>
              <List>
                <ListItem button>
                  <ListItemText primary="chatbox1"/>
                </ListItem>
                <ListItem button>
                  <ListItemText primary="chatbox2"/>
                </ListItem>
                <ListItem button>
                  <ListItemText primary="chatbox3"/>
                </ListItem>
                <ListItem button>
                  <ListItemText primary="chatbox4"/>
                </ListItem>
              </List>
            </RctRightSideMenuComponent>
          }
        >
          <div>MY CONTENT</div>
        </RctLayoutComponent>
      </BrowserRouter>
    </Provider>
  ))
storiesOf('RctHeaderComponent', module)
  .add('with layout and side-menu', () => (
    <Provider { ...stores }>
      <BrowserRouter>
        <RctLayoutComponent topComponent={ <RctHeaderComponent logo={ Logo } /> } sideMenuComponent={ <RctSideMenuComponent/> } />
      </BrowserRouter>
    </Provider>
  ))
storiesOf('RctSearchInputComponent', module)
  .add('simple usage', () => (
    <Grid container justify="flex-end" align="center" style={{
      backgroundColor: blue[500],
      height: '100px',
    }} >
      <Grid item>
        <RctSearchInputComponent/>
      </Grid>
    </Grid>
  ))
storiesOf('RctProjectComponent', module)
  .add('with layout, side-menu and switch', () => (
    <Provider { ...stores }>
      <App>
        <BrowserRouter>
          <RctProjectComponent
            layoutComponent={
              <RctLayoutComponent
                topComponent={ <RctHeaderComponent logo={ Logo } /> }
                sideMenuComponent={
                  <RctSideMenuComponent>
                    <SideMenuItem icon="home" label="Home" route="/" divider />
                    <SideMenuItem icon="account_box" label="Login" route="/login" />
                    <SideMenuItem icon="fiber_new" label="Register" route="/register" />
                  </RctSideMenuComponent>
                }
              />
            }
          >
            <Switch>
              <Route path="/login" component={ RctLoginComponent } />
              <Route path="/register" component={ RctRegisterComponent } />
              <Route path="/" component={ HomeComponent } />
            </Switch>
          </RctProjectComponent>
        </BrowserRouter>
      </App>
    </Provider>
  ))
storiesOf('RctSideMenuComponent', module)
  .add('simple usage', () => (
    <BrowserRouter>
      <RctSideMenuComponent>
        <SideMenuItem icon="inbox" label="Inbox" route="/" />
        <SideMenuItem icon="star" label="Starred" route="/" />
        <SideMenuItem icon="send" label="Send mail" route="/" />
        <SideMenuItem icon="drafts" label="Drafts" divider route="/" />
        <SideMenuItem icon="email" label="All mail" route="/" />
        <SideMenuItem icon="delete" label="Trash" route="/" />
        <SideMenuItem icon="report" label="Spam" route="/" />
      </RctSideMenuComponent>
    </BrowserRouter>
  ))
storiesOf('RctFooterComponent', module)
  .add('simple usage', () => (
    <RctFooterComponent>
      <FooterItem icon="restore" label="Recents"/>
      <FooterItem icon="favorite" label="Favorites"/>
    </RctFooterComponent>
  ))
storiesOf('RctNotificationComponent', module)
  .addDecorator(centered)
  .add('with login', () => (
    <BrowserRouter>
      <Provider { ...stores }>
        <App>
          <RctLoginComponent/>
          <RctNotificationComponent/>
        </App>
      </Provider>
    </BrowserRouter>
  ))
storiesOf('RctRightSideMenuComponent', module)
  .add('simple usage', () => (
    <BrowserRouter>
      <RctRightSideMenuComponent contentComponent= {<Typography type='body1' noWrap>
       {'You think water moves fast? You should see ice.'}
      </Typography>} >
        <List>
          <ListItem button>
            <ListItemText primary="chatbox1"/>
          </ListItem>
          <ListItem button>
            <ListItemText primary="chatbox2"/>
          </ListItem>
          <ListItem button>
            <ListItemText primary="chatbox3"/>
          </ListItem>
          <ListItem button>
            <ListItemText primary="chatbox4"/>
          </ListItem>
        </List>
      </RctRightSideMenuComponent>
    </BrowserRouter>
  ))
  .add('with chatboxList', () => (
    <Provider { ...stores }>
      <App>
        <RctRightSideMenuComponent
          contentComponent={
            <Typography type='body1' noWrap>
              You think water moves fast? You should see ice.
            </Typography>
          }
        >
          <RctChatboxListComponent/>
        </RctRightSideMenuComponent>
      </App>
    </Provider>
  ))
  .add('with eventList', () => (
    <Provider { ...stores } >
      <APIApp>
        <RctRightSideMenuComponent
          contentComponent={
            <Typography type='body1' noWrap>
              You think water moves fast? You should see ice.
            </Typography>
          }
        >
          <RctEventListComponent
            merchandId={ process.env.STORYBOOK_TOKEN }
            bookingUrl={ process.env.STORYBOOK_BOOKING_URL }
            topTitle='Book your Adventure Today!'
          />
        </RctRightSideMenuComponent>
      </APIApp>
    </Provider>
  ))
storiesOf('RctChatboxListComponent', module)
  .addDecorator(centered)
  .add('simple usage', () => (
    <Provider { ...stores }>
      <App>
        <RctChatboxListComponent/>
      </App>
    </Provider>
  ))
storiesOf('RctChatboxGridComponent', module)
  .add('simple usage', () => (
    <Provider { ...stores }>
      <App>
        <RctChatboxGridComponent/>
      </App>
    </Provider>
  ))
  .add('chatboxgrid with broadcast message component', () => (
    <Provider { ...stores }>
      <App>
        <RctChatboxGridComponent/>
        <RctBroadcastMessageComponent helper="Send a message to all active chatboxes!" label="Your message"/>
      </App>
    </Provider>
  ))
storiesOf('RctBroadcastMessageComponent', module)
  .add('simple usage', () => (
    <Provider { ...stores }>
      <BrowserRouter>
        <App>
          <RctBroadcastMessageComponent helper="Send a message to all active chatboxes !" label="Your message"/>
        </App>
      </BrowserRouter>
    </Provider>
  )
)
storiesOf('RctEventListComponent', module)
  .add('simple usage', () => (
    <Provider { ...stores } >
      <APIApp>
        <RctEventListComponent
          merchandId={ process.env.STORYBOOK_API_APP_TOKEN }
          bookingUrl={ process.env.STORYBOOK_BOOKING_URL }
          topTitle='Book your Adventure Today!'
        />
      </APIApp>
    </Provider>
  )
)
storiesOf('RctDatePickerComponent', module)
  .add('simple usage', () => (
    <Provider { ...stores }>
      <BrowserRouter>
        <RctDatePickerComponent months={1} placeholder='Choose date' calendarIcon/>
      </BrowserRouter>
    </Provider>
  )
)
storiesOf('RctReorderableGridListComponent', module)
  .add('simple usage', () => (
    <Provider { ...stores }>
      <WrapReorderableGridListComponent/>
    </Provider>
  ))
storiesOf('RctNotificationMenuComponent', module)
  .addDecorator(centered)
  .add('simple usage', () => (
    <BrowserRouter>
      <Provider { ...stores }>
        <App>
          <RctNotificationMenuComponent/>
        </App>
      </Provider>
    </BrowserRouter>
  ))
storiesOf('RctAccountComponent', module)
  .addDecorator(centered)
  .add('simple usage',
    withInfo('This is the basic usage of the component without any props provided')(() => (
      <BrowserRouter>
        <Provider { ...stores }>
          <App>
            <RctAccountComponent cardWidth='400px' />
          </App>
        </Provider>
      </BrowserRouter>
    ))
  )
