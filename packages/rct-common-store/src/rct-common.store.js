import { observable, action, reaction } from 'mobx'
import swaggerClientStore from '@4geit/rct-swagger-client-store'

class RctCommonStore {
  @observable appName = 'Test'
  @observable token = window.localStorage.getItem('token')
  @observable user
  @observable isLoggedIn = false
  @observable appLoaded = false

  constructor() {
    reaction(
      () => this.token,
      token => {
        if (token) {
          window.localStorage.setItem('token', token)
        } else {
          window.localStorage.removeItem('token')
        }
      }
    )
  }

  @action setToken(token) {
    this.token = token
    this.isLoggedIn = true
  }
  @action setAppLoaded() {
    this.appLoaded = true
  }
  @action logout() {
    window.localStorage.removeItem('token')
    this.token = undefined
    this.isLoggedIn = false
  }
  @action setUser(user) {
    this.user = user
  }
  @action restoreSession(accountOperationId, token) {
    accountOperationId = accountOperationId || 'account'
    this.inProgress = true
    try {
      const token = this.token
      if (!token) {
        this.inProgress = false
        return
      }
      runInAction(() => {
        await swaggerClientStore.buildClientWithToken({token})
        const { client: { apis: { Account } } } = swaggerClientStore
        const { body: { firstname, lastname, email: emailField, password: passwordField, company, address, phone } } = await Account[accountOperationId]()
        this.setUser({ firstname, lastname, email: emailField, password: passwordField, company, address, phone })
        this.inProgress = false
      })

    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctCommonStore()
