# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)


### Features

* **Account Store:** create fetchData method to get user info ([461c460](https://gitlab.com/4geit/react-packages/commit/461c460))




<a name="1.71.0"></a>
# [1.71.0](https://gitlab.com/4geit/react-packages/compare/v1.70.2...v1.71.0) (2017-10-11)


### Bug Fixes

* **header component and common store:** minor fix ([b2885a3](https://gitlab.com/4geit/react-packages/commit/b2885a3))




<a name="1.70.0"></a>
# [1.70.0](https://gitlab.com/4geit/react-packages/compare/v1.69.1...v1.70.0) (2017-10-10)


### Bug Fixes

* **header component:** added methods to authstore, commonstore and component ([48c3542](https://gitlab.com/4geit/react-packages/commit/48c3542))
* **header component:** code fix ([ebc2492](https://gitlab.com/4geit/react-packages/commit/ebc2492))




<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.24.0"></a>
# [1.24.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.24.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.23.0"></a>
# [1.23.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.23.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/4geit/react-packages/compare/v1.6.8...v1.6.9) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/4geit/react-packages/compare/v1.6.7...v1.6.8) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/4geit/react-packages/compare/v1.6.6...v1.6.7) (2017-08-26)


### Bug Fixes

* **template:** fix minor issue ([a290cb9](https://gitlab.com/4geit/react-packages/commit/a290cb9))




<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/4geit/react-packages/compare/v1.6.5...v1.6.6) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/4geit/react-packages/compare/v1.6.4...v1.6.5) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-common-store

<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/4geit/react-packages/compare/1.6.3...1.6.4) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-common-store
