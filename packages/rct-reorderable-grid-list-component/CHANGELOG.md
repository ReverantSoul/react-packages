# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.61.0"></a>
# [1.61.0](https://gitlab.com/4geit/react-packages/compare/v1.60.0...v1.61.0) (2017-10-06)


### Features

* **rct-reorderable-grid-list:** pass `item` as a prop to grid-tile ([d170145](https://gitlab.com/4geit/react-packages/commit/d170145))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.45.0"></a>
# [1.45.0](https://gitlab.com/4geit/react-packages/compare/v1.44.5...v1.45.0) (2017-10-02)


### Bug Fixes

* **ChatboxGridComponent:** css minor change ([536b7de](https://gitlab.com/4geit/react-packages/commit/536b7de))


### Features

* **ChatboxGrid:** Added Avatar, minor css change ([031b032](https://gitlab.com/4geit/react-packages/commit/031b032))
* **style chatbox:** added css ([5acf043](https://gitlab.com/4geit/react-packages/commit/5acf043))
* **Style itemcomponent:** css ([f6bdbc6](https://gitlab.com/4geit/react-packages/commit/f6bdbc6))




<a name="1.44.3"></a>
## [1.44.3](https://gitlab.com/4geit/react-packages/compare/v1.44.2...v1.44.3) (2017-09-27)


### Bug Fixes

* **reorderable-grid-list:** fix setPosition call with destructured param instead of fixed params ([3e64768](https://gitlab.com/4geit/react-packages/commit/3e64768))




<a name="1.42.0"></a>
# [1.42.0](https://gitlab.com/4geit/react-packages/compare/v1.41.0...v1.42.0) (2017-09-22)


### Features

* **rct-chatbox-grid-component:** start integrating reorderable-grid-list-component ([9e4febd](https://gitlab.com/4geit/react-packages/commit/9e4febd))
* **rct-reorderable-grid-list-component:** add itemComponent prop to set component to each tile of t ([5157ae0](https://gitlab.com/4geit/react-packages/commit/5157ae0))
* **rct-reorderable-grid-list-component:** tile component now works with source and target, remove s ([75a6257](https://gitlab.com/4geit/react-packages/commit/75a6257))
* **reorderable-grid-list-component:** add a new reorderable grid list component + refactor componen ([09fa0da](https://gitlab.com/4geit/react-packages/commit/09fa0da))
