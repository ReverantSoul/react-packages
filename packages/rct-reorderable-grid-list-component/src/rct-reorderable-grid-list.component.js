import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, computed, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import { DragDropContext, DragSource, DropTarget } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import { GridList, GridListTile } from 'material-ui/GridList'

import './rct-reorderable-grid-list.component.css'

@DragSource('tile', {
  beginDrag(props) {
    return { ...props }
  }
}, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
}))
@DropTarget('tile', {
  drop(props, monitor, component) {
    const source = monitor.getItem()
    const target = props
    const { props: { handleSetPosition, handleFetchData } } = component
    if (monitor.didDrop) {
      handleSetPosition({
        itemId: source.itemId,
        position: target.position,
      })
        .then(() => handleSetPosition({
          itemId: target.itemId,
          position: source.position < target.position ? target.position - 1 : target.position + 1,
        }))
        .then(() => handleFetchData())
    }
  }
}, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
}))
@propTypes({
  connectDragSource: PropTypes.func.isRequired,
  connectDropTarget: PropTypes.func.isRequired,
  isDragging: PropTypes.bool.isRequired,
  isOver: PropTypes.bool.isRequired,
  itemId: PropTypes.any.isRequired,
  item: PropTypes.any.isRequired,
  position: PropTypes.number.isRequired,
  handleSetPosition: PropTypes.func.isRequired,
  handleFetchData: PropTypes.func.isRequired,
  itemComponent: PropTypes.node.isRequired,
})
export class ReorderableGridListTile extends Component {
  render() {
    const { connectDragSource, isDragging, connectDropTarget, isOver, style, style: { height, padding }, itemComponent, ...props } = this.props
    const _itemComponent = React.cloneElement(itemComponent, { ...props })
    return (
      connectDragSource(
        connectDropTarget(
          <div style={ {
            opacity: isDragging ? .5 : 1,
            cursor: 'move',
          } } >
            <GridListTile
              style={ {
              ...style,
            } }
            >
              { _itemComponent }
            </GridListTile>
          </div>
        )
      )
    )
  }
}

@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
@DragDropContext(HTML5Backend)
// @inject('xyzStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  handleSetPosition: PropTypes.func.isRequired,
  handleFetchData: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
  itemComponent: PropTypes.node.isRequired,
  // TBD
})
@defaultProps({
  // TBD
})
export default class RctReorderableGridListComponent extends Component {
  render() {
    const { handleSetPosition, handleFetchData, itemComponent, data, ...props } = this.props
    return (
      <GridList { ...props } >
        { data.map((item, index) => {
          const { id } = item
          return (
            <ReorderableGridListTile
              key={ id }
              itemId={ id }
              item={ item }
              position={ index }
              handleSetPosition={ handleSetPosition }
              handleFetchData={ handleFetchData }
              itemComponent={ itemComponent }
              style={ {
                width: "auto",
                height: "auto",
              } }
            />
          )
        }) }
      </GridList>
    )
  }
}
