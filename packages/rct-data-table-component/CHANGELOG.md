# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.69.0"></a>
# [1.69.0](https://gitlab.com/4geit/react-packages/compare/v1.68.0...v1.69.0) (2017-10-10)


### Features

* **data-model:** import feature implemented ([d70f23f](https://gitlab.com/4geit/react-packages/commit/d70f23f))




<a name="1.68.0"></a>
# [1.68.0](https://gitlab.com/4geit/react-packages/compare/v1.67.1...v1.68.0) (2017-10-09)


### Features

* **data-table:** add import dialog + file input and logic to convert csv to json ([10eafd6](https://gitlab.com/4geit/react-packages/commit/10eafd6))




<a name="1.66.0"></a>
# [1.66.0](https://gitlab.com/4geit/react-packages/compare/v1.65.0...v1.66.0) (2017-10-09)


### Features

* **data-table:** add global delete button logic ([6883242](https://gitlab.com/4geit/react-packages/commit/6883242))




<a name="1.65.0"></a>
# [1.65.0](https://gitlab.com/4geit/react-packages/compare/v1.64.0...v1.65.0) (2017-10-07)


### Features

* **data-table:** add multi-selection ([6b6d69e](https://gitlab.com/4geit/react-packages/commit/6b6d69e))




<a name="1.63.0"></a>
# [1.63.0](https://gitlab.com/4geit/react-packages/compare/v1.62.1...v1.63.0) (2017-10-06)


### Features

* **data-table:** add reload of data when add, edit and remove actions triggered + add snackbar to u ([7c89c92](https://gitlab.com/4geit/react-packages/commit/7c89c92))




<a name="1.62.1"></a>
## [1.62.1](https://gitlab.com/4geit/react-packages/compare/v1.62.0...v1.62.1) (2017-10-06)


### Bug Fixes

* **data-table:** fix menu issue thanks [@geraldinestarke](https://gitlab.com/geraldinestarke) ;-) ([c370609](https://gitlab.com/4geit/react-packages/commit/c370609))




<a name="1.60.0"></a>
# [1.60.0](https://gitlab.com/4geit/react-packages/compare/v1.59.0...v1.60.0) (2017-10-05)


### Features

* **data-table-component:** add logic for add and edit button + dialogs ([cd1a035](https://gitlab.com/4geit/react-packages/commit/cd1a035))




<a name="1.58.0"></a>
# [1.58.0](https://gitlab.com/4geit/react-packages/compare/v1.57.0...v1.58.0) (2017-10-05)


### Features

* **data-table:** add columns as text field in add form dialog ([2f4742c](https://gitlab.com/4geit/react-packages/commit/2f4742c))




<a name="1.57.0"></a>
# [1.57.0](https://gitlab.com/4geit/react-packages/compare/v1.56.0...v1.57.0) (2017-10-05)


### Features

* **data-table:** wire add button with a form dialog ([22aee2d](https://gitlab.com/4geit/react-packages/commit/22aee2d))




<a name="1.55.0"></a>
# [1.55.0](https://gitlab.com/4geit/react-packages/compare/v1.54.0...v1.55.0) (2017-10-05)


### Features

* **Data table Store:** Add a store method removeItem ([901b541](https://gitlab.com/4geit/react-packages/commit/901b541))
* **storybook:** add MUI Menu component test ([b9690e3](https://gitlab.com/4geit/react-packages/commit/b9690e3))




<a name="1.53.0"></a>
# [1.53.0](https://gitlab.com/4geit/react-packages/compare/v1.52.1...v1.53.0) (2017-10-04)


### Bug Fixes

* **Data table:** add Logic ([7cf83fc](https://gitlab.com/4geit/react-packages/commit/7cf83fc))
* **Data Table:** minor fix ([86757fa](https://gitlab.com/4geit/react-packages/commit/86757fa))
* **DataTable:** added constant {id}=item ([530ecc2](https://gitlab.com/4geit/react-packages/commit/530ecc2))


### Features

* **Data table:** call store method remoItem ([45282b7](https://gitlab.com/4geit/react-packages/commit/45282b7))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.50.0"></a>
# [1.50.0](https://gitlab.com/4geit/react-packages/compare/v1.49.0...v1.50.0) (2017-10-04)


### Features

* **DataTable:** Add Remove button to data table ([472be8d](https://gitlab.com/4geit/react-packages/commit/472be8d))




<a name="1.49.0"></a>
# [1.49.0](https://gitlab.com/4geit/react-packages/compare/v1.48.2...v1.49.0) (2017-10-03)


### Bug Fixes

* **DataTable:** Add Edit button ([9f22f88](https://gitlab.com/4geit/react-packages/commit/9f22f88))
* **DataTable:** Edit button mior fix ([8a56f28](https://gitlab.com/4geit/react-packages/commit/8a56f28))


### Features

* **DataTable:** Add Edit button and click event ([1956374](https://gitlab.com/4geit/react-packages/commit/1956374))




<a name="1.48.0"></a>
# [1.48.0](https://gitlab.com/4geit/react-packages/compare/v1.47.1...v1.48.0) (2017-10-03)


### Bug Fixes

* **Datatable:** add icon ([2b2f16b](https://gitlab.com/4geit/react-packages/commit/2b2f16b))


### Features

* **DataTable:** add buton ([6c73e29](https://gitlab.com/4geit/react-packages/commit/6c73e29))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.44.1"></a>
## [1.44.1](https://gitlab.com/4geit/react-packages/compare/v1.44.0...v1.44.1) (2017-09-26)


### Bug Fixes

* **data-table:** fix columns menu issue ([dbac9e2](https://gitlab.com/4geit/react-packages/commit/dbac9e2))




<a name="1.44.0"></a>
# [1.44.0](https://gitlab.com/4geit/react-packages/compare/v1.43.1...v1.44.0) (2017-09-26)


### Features

* **Import action Button:** Added icon ([34e111e](https://gitlab.com/4geit/react-packages/commit/34e111e))




<a name="1.41.0"></a>
# [1.41.0](https://gitlab.com/4geit/react-packages/compare/v1.40.1...v1.41.0) (2017-09-22)


### Bug Fixes

* **Buttons:** Delete and checkbox buttons implementation ([d7e1f4b](https://gitlab.com/4geit/react-packages/commit/d7e1f4b))
* **fix:** code fix ([0d9dee2](https://gitlab.com/4geit/react-packages/commit/0d9dee2))


### Features

* **General Delete Button:** Add a checkbox next to each item of the data datle and a general delete ([ac47fce](https://gitlab.com/4geit/react-packages/commit/ac47fce))




<a name="1.35.0"></a>
# [1.35.0](https://gitlab.com/4geit/react-packages/compare/v1.34.4...v1.35.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.34.2"></a>
## [1.34.2](https://gitlab.com/4geit/react-packages/compare/v1.34.1...v1.34.2) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.26.0"></a>
# [1.26.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.26.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.25.0"></a>
# [1.25.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.25.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.21.1"></a>
## [1.21.1](https://gitlab.com/4geit/react-packages/compare/v1.23.0...v1.21.1) (2017-09-08)


### Bug Fixes

* **datatable:** minor fix ([0ae2610](https://gitlab.com/4geit/react-packages/commit/0ae2610))




<a name="1.21.0"></a>
# [1.21.0](https://gitlab.com/4geit/react-packages/compare/v1.20.0...v1.21.0) (2017-09-08)


### Features

* **datatable:** add operationId and enabledColumns props and fetchData method ([760bf97](https://gitlab.com/4geit/react-packages/commit/760bf97))




<a name="1.20.0"></a>
# [1.20.0](https://gitlab.com/4geit/react-packages/compare/v1.18.0...v1.20.0) (2017-09-08)


### Features

* **data-table-component:** add prop title to data-table-component ([b8666bb](https://gitlab.com/4geit/react-packages/commit/b8666bb))




<a name="1.19.0"></a>
# [1.19.0](https://gitlab.com/4geit/react-packages/compare/v1.18.0...v1.19.0) (2017-09-08)


### Features

* **data-table-component:** add prop title to data-table-component ([b8666bb](https://gitlab.com/4geit/react-packages/commit/b8666bb))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.15"></a>
## [1.6.15](https://gitlab.com/4geit/react-packages/compare/v1.6.14...v1.6.15) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/4geit/react-packages/compare/v1.6.8...v1.6.9) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/4geit/react-packages/compare/v1.6.7...v1.6.8) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/4geit/react-packages/compare/v1.6.6...v1.6.7) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/4geit/react-packages/compare/v1.6.5...v1.6.6) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/4geit/react-packages/compare/v1.6.4...v1.6.5) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component

<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/4geit/react-packages/compare/1.6.3...1.6.4) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-component
