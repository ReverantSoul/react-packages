import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import csvtojson from 'csvtojson'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Table, { TableBody, TableCell, TableHead, TableRow, TableSortLabel } from 'material-ui/Table'
import Menu, { MenuItem } from 'material-ui/Menu'
import Switch from 'material-ui/Switch'
import Paper from 'material-ui/Paper'
import Typography from 'material-ui/Typography'
import Checkbox from 'material-ui/Checkbox'
import Grid from 'material-ui/Grid'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete';
import AddIcon from 'material-ui-icons/Add';
import EditIcon from 'material-ui-icons/Edit';
import ImportExportIcon from 'material-ui-icons/ImportExport'
import ViewColumnIcon from 'material-ui-icons/ViewColumn'
import FilterListIcon from 'material-ui-icons/FilterList'
import Dialog, { DialogActions, DialogContent, DialogContentText, DialogTitle } from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'
import Snackbar from 'material-ui/Snackbar'

import './rct-data-table.component.css'

@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('dataTableStore', 'notificationStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  listOperationId: PropTypes.string.isRequired,
  addOperationId: PropTypes.string.isRequired,
  updateOperationId: PropTypes.string.isRequired,
  deleteOperationId: PropTypes.string.isRequired,
  bulkAddOperationId: PropTypes.string.isRequired,
  enabledColumns: PropTypes.array,
  // TBD
})
@defaultProps({
  title: 'Data Table',
  enabledColumns: [],
  // TBD
})
export default class RctDataTableComponent extends Component {
  async componentWillMount() {
    const { listOperationId, enabledColumns, dataTableStore } = this.props
    await dataTableStore.fetchData({ listOperationId, enabledColumns })
  }
  componentWillUnmount() {
    this.props.dataTableStore.reset()
  }
  handleOpenAddDialogClick() {
    const { dataTableStore } = this.props
    dataTableStore.mutableColumns.map(({ name }) => {
      dataTableStore.setAddDialogField(name, '')
    })
    dataTableStore.setAddDialogOpen(true)
  }
  handleOpenEditDialogClick = (item) => () => {
    const { dataTableStore } = this.props
    dataTableStore.setEditDialogOpen(true)
    dataTableStore.setEditDialogItem(item)
  }
  handleOpenImportDialogClick() {
    const { dataTableStore } = this.props
    dataTableStore.setImportDialogOpen(true)
  }
  handleAddDialogInputChange({ target: { name, value } }) {
    this.props.dataTableStore.setAddDialogField(name, value)
  }
  handleEditDialogInputChange({ target: { name, value } }) {
    this.props.dataTableStore.setEditDialogField(name, value)
  }
  async handleAddClick() {
    const { dataTableStore, enabledColumns, listOperationId, addOperationId } = this.props
    const { addDialogFields: body } = dataTableStore
    await dataTableStore.addItem({
      addOperationId,
      body,
    }).then(() => {
      this.handleAddDialogRequestClose()
    })
    await dataTableStore.fetchData({ listOperationId, enabledColumns })
  }
  async handleEditClick() {
    const { dataTableStore, enabledColumns, listOperationId, updateOperationId } = this.props
    const { editDialogFields: body, editDialogItem: { id } } = dataTableStore
    await dataTableStore.editItem({
      updateOperationId,
      id,
      body,
    })
    await dataTableStore.fetchData({ listOperationId, enabledColumns })
    this.handleEditDialogRequestClose()
  }
  async handleImportClick() {
    const { dataTableStore, enabledColumns, listOperationId, bulkAddOperationId } = this.props
    const { importedData } = dataTableStore
    await dataTableStore.importItem({
      bulkAddOperationId,
      body: importedData.slice(),
    })
    await dataTableStore.fetchData({ listOperationId, enabledColumns })
    this.handleImportDialogRequestClose()
  }
  handleOpenColumnsMenuClick(event) {
    const { dataTableStore } = this.props
    dataTableStore.setColumnsMenuOpen(true)
    dataTableStore.setColumnsMenuElement(event.currentTarget)
  }
  handleColumnsMenuRequestClose() {
    this.props.dataTableStore.setColumnsMenuOpen(false)
  }
  handleAddDialogRequestClose() {
    this.props.dataTableStore.setAddDialogOpen(false)
  }
  handleEditDialogRequestClose() {
    this.props.dataTableStore.setEditDialogOpen(false)
  }
  handleImportDialogRequestClose() {
    this.props.dataTableStore.setImportDialogOpen(false)
  }
  handleColumnItemChange(item) {
    return (event, checked) => item.setEnabled(checked)
  }
  @action handleSelectAllClick(event, checked) {
    const { dataTableStore } = this.props
    const { data } = dataTableStore
    if (checked) {
      dataTableStore.setSelected(data.map(({ id }) => id))
      return
    }
    dataTableStore.setSelected([])
  }
  @action handleSelectClick(event, id) {
    const { dataTableStore } = this.props
    const { selected } = dataTableStore
    const selectedIndex = selected.indexOf(id)
    let newSelected = []
    if (selectedIndex === -1) { // when index not selected yet, add it
      newSelected = newSelected.concat(selected.slice(), id)
    } else if (selectedIndex === 0) { // when index is the first item
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) { // when index is the last item
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) { // when index is between
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      )
    }
    dataTableStore.setSelected(newSelected)
  }
  isSelected(id) {
    const { dataTableStore: { selected } } = this.props
    return !!~selected.indexOf(id)
  }
  handleRemoveSnackbarOpen = ({ id }) => () => {
    const { dataTableStore } = this.props
    dataTableStore.setRemoveSnackbarOpen(true)
    dataTableStore.setRemoveId(id)
  }
  async handleRemoveSnackbarRequestClose(event, reason) {
    const { dataTableStore, enabledColumns, listOperationId, deleteOperationId } = this.props
    const { removeId } = dataTableStore
    if (reason === 'timeout') {
      console.log(`remove id ${removeId}`)
      await dataTableStore.removeItem({ id: removeId, deleteOperationId })
      await dataTableStore.fetchData({ listOperationId, enabledColumns })
    }
    dataTableStore.setRemoveSnackbarOpen(false)
    dataTableStore.setRemoveId(undefined)
  }
  handleRemoveAllSnackbarOpen() {
    const { dataTableStore } = this.props
    const { selected } = dataTableStore
    dataTableStore.setRemoveAllSnackbarOpen(true)
    dataTableStore.setRemoveAllIds(selected)
    dataTableStore.setSelected([])
  }
  async handleRemoveAllSnackbarRequestClose(event, reason) {
    const { dataTableStore, enabledColumns, listOperationId, deleteOperationId } = this.props
    const { removeAllIds } = dataTableStore
    if (reason === 'timeout') {
      console.log(`remove all ids ${removeAllIds}`)
      await Promise.all(removeAllIds.map(async (removeId) => {
        await dataTableStore.removeItem({ id: removeId, deleteOperationId })
      }))
      await dataTableStore.fetchData({ listOperationId, enabledColumns })
    }
    dataTableStore.setRemoveAllSnackbarOpen(false)
    dataTableStore.setRemoveAllIds([])
  }
  handleReadImportedFile(event) {
    const { dataTableStore, notificationStore } = this.props
    const { mutableColumns } = dataTableStore
    const [ file ] = event.target.files
    const reader = new FileReader()
    reader.readAsText(file)
    reader.onload = event => {
      const { result } = event.target
      csvtojson()
        .fromString(result)
        .on('json', obj => {
          const _keys = Object.keys(obj)
          if (mutableColumns.some(({ name }) => !_keys.includes(name))) {
            notificationStore.newMessage('bad format')
            throw new Error('bad format')
          }
        })
        .on('end_parsed', data => {
          dataTableStore.setImportedData(data)
        })
    }
  }

  render() {
    const { title, classes, dataTableStore } = this.props
    const { columnsMenuOpen, columnsMenuElement, addDialogOpen, editDialogOpen, importDialogOpen, addDialogFields, editDialogFields, editDialogItem, removeSnackbarOpen, removeAllSnackbarOpen, columns, enabledColumns, mutableColumns, data, selected } = dataTableStore

    if (!data.length) {
      return (
        <Typography>Loading datatable...</Typography>
      )
    }
    return (
      <div>
        <Paper>
          <Toolbar>
            {/* title */}
            <Typography type="title" color="inherit" style={{
              flex: 1
            }}>
              { title }
            </Typography>
            {/* add button */}
            <IconButton onClick={ this.handleOpenAddDialogClick.bind(this) }>
              <AddIcon/>
            </IconButton>
            {/* import button */}
            <IconButton onClick={ this.handleOpenImportDialogClick.bind(this) }>
              <ImportExportIcon/>
            </IconButton>
            {/* columns button */}
            <IconButton
              onClick={ this.handleOpenColumnsMenuClick.bind(this) }
            >
              <ViewColumnIcon/>
            </IconButton>
            {/* columns menu */}
            <Menu
              anchorEl={ columnsMenuElement }
              open={ columnsMenuOpen }
              onRequestClose={ this.handleColumnsMenuRequestClose.bind(this) }
              className="menu"
            >
              { columns.map((item) => (
                <MenuItem key={ item.name } >
                  <Grid container alignItems="center">
                    <Grid item xs>
                      <Typography>{ item.name }</Typography>
                    </Grid>
                    <Grid item>
                      <Switch
                        checked={ item.enabled }
                        onChange={ this.handleColumnItemChange(item) }
                      />
                    </Grid>
                  </Grid>
                </MenuItem>
              )) }
            </Menu>
            {/* delete button */}
            <IconButton onClick={ this.handleRemoveAllSnackbarOpen.bind(this) }>
              <DeleteIcon/>
            </IconButton>
            {/* add form dialog */}
            <Dialog open={ addDialogOpen } onRequestClose={ this.handleAddDialogRequestClose.bind(this) }>
              <DialogTitle>Add a new item</DialogTitle>
              <DialogContent>
                <DialogContentText>Complete the following form and click on the Add button in order to add a new item.</DialogContentText>
                { mutableColumns.map(({ name }) => {
                  return (
                    <TextField
                      key={ name }
                      name={ name }
                      label={ name }
                      type="text"
                      fullWidth
                      onChange={ this.handleAddDialogInputChange.bind(this) }
                    />
                  )
                })}
              </DialogContent>
              <DialogActions>
                <Button onClick={ this.handleAddDialogRequestClose.bind(this) }>Cancel</Button>
                <Button onClick={ this.handleAddClick.bind(this) } color="primary">Add</Button>
              </DialogActions>
            </Dialog>
            {/* edit form dialog */}
            <Dialog open={ editDialogOpen } onRequestClose={ this.handleEditDialogRequestClose.bind(this) }>
              <DialogTitle>Edit an item</DialogTitle>
              <DialogContent>
                <DialogContentText>You can edit the fields below and proceed with the button Update.</DialogContentText>
                { mutableColumns.map(({ name }) => {
                  return (
                    <TextField
                      key={ name }
                      name={ name }
                      label={ name }
                      type="text"
                      fullWidth
                      value={ editDialogItem && editDialogItem[name] }
                    />
                  )
                })}
              </DialogContent>
              <DialogActions>
                <Button onClick={ this.handleEditDialogRequestClose.bind(this) }>Cancel</Button>
                <Button onClick={ this.handleEditClick.bind(this) } color="primary">Update</Button>
              </DialogActions>
            </Dialog>
            {/* import form dialog */}
            <Dialog open={ importDialogOpen } onRequestClose={ this.handleImportDialogRequestClose.bind(this) }>
              <DialogTitle>Import items</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  <span>You can import a CSV file. The CSV file should contains the following fields:</span>
                </DialogContentText>
                { mutableColumns.map(({ name }) => (
                  <Typography key={ name }>* { name }</Typography>
                )) }
                <TextField fullWidth type="file" onChange={ this.handleReadImportedFile.bind(this) } />
              </DialogContent>
              <DialogActions>
                <Button onClick={ this.handleImportDialogRequestClose.bind(this) }>Cancel</Button>
                <Button onClick={ this.handleImportClick.bind(this) } color="primary">Upload</Button>
              </DialogActions>
            </Dialog>
          </Toolbar>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    indeterminate={ selected.length > 0 && selected.length < data.length }
                    checked={ selected.length === data.length }
                    onChange={ this.handleSelectAllClick.bind(this) }
                  />
                </TableCell>
                { enabledColumns.map(({ name }) => (
                  <TableCell key={ name }>{ name }</TableCell>
                )) }
                <TableCell>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              { data.map(item => {
                const { id } = item
                const isSelected = this.isSelected(id)
                return (
                  <TableRow hover key={ id } >
                    <TableCell padding="checkbox">
                      <Checkbox
                        checked={ isSelected }
                        onChange={ event => this.handleSelectClick(event, id) }
                      />
                    </TableCell>
                    { enabledColumns.map(({ name }) => (
                      <TableCell key={ name } >{ item[name] }</TableCell>
                    )) }
                    <TableCell>
                      {/* edit button */}
                      <IconButton onClick={ this.handleOpenEditDialogClick(item) }>
                        <EditIcon/>
                      </IconButton>
                      {/* delete button */}
                      <IconButton onClick={ this.handleRemoveSnackbarOpen({ id })}>
                        <DeleteIcon/>
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </Paper>
        <Snackbar
          open={ removeSnackbarOpen }
          autoHideDuration={ 5000 }
          message={ <span>Item removed!</span> }
          onRequestClose={ this.handleRemoveSnackbarRequestClose.bind(this) }
          action={[
            <Button key="undo" color="accent" dense onClick={ this.handleRemoveSnackbarRequestClose.bind(this) }>Undo</Button>
          ]}
        />
        <Snackbar
          open={ removeAllSnackbarOpen }
          autoHideDuration={ 5000 }
          message={ <span>Items removed!</span> }
          onRequestClose={ this.handleRemoveAllSnackbarRequestClose.bind(this) }
          action={[
            <Button key="undo" color="accent" dense onClick={ this.handleRemoveAllSnackbarRequestClose.bind(this) }>Undo</Button>
          ]}
        />
      </div>
    )
  }
}
