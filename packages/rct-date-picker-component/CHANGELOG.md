# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.70.1"></a>
## [1.70.1](https://gitlab.com/4geit/react-packages/compare/v1.70.0...v1.70.1) (2017-10-10)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.39.4"></a>
## [1.39.4](https://gitlab.com/4geit/react-packages/compare/v1.39.3...v1.39.4) (2017-09-21)




**Note:** Version bump only for package @4geit/rct-date-picker-component

<a name="1.39.2"></a>
## [1.39.2](https://gitlab.com/4geit/react-packages/compare/v1.39.1...v1.39.2) (2017-09-21)


### Bug Fixes

* **date-picker:** fix proper use of action method of the store ([d90f3d2](https://gitlab.com/4geit/react-packages/commit/d90f3d2))
* **date-picker-component:** fix issue with observable assignment ([5ddf8f4](https://gitlab.com/4geit/react-packages/commit/5ddf8f4))




<a name="1.39.0"></a>
# [1.39.0](https://gitlab.com/4geit/react-packages/compare/v1.38.0...v1.39.0) (2017-09-21)


### Bug Fixes

* **date-picker-component:** remove button and add it to eventListComponent ([d474352](https://gitlab.com/4geit/react-packages/commit/d474352))
* **date-picker-container:** add state and search button ([203155f](https://gitlab.com/4geit/react-packages/commit/203155f))
* **minor:** minor ([0e2f966](https://gitlab.com/4geit/react-packages/commit/0e2f966))


### Features

* **date-picker-component:** add the single date picker component ([697c112](https://gitlab.com/4geit/react-packages/commit/697c112))
