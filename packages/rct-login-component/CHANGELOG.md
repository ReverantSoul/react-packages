# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.71.0"></a>
# [1.71.0](https://gitlab.com/4geit/react-packages/compare/v1.70.2...v1.71.0) (2017-10-11)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.70.0"></a>
# [1.70.0](https://gitlab.com/4geit/react-packages/compare/v1.69.1...v1.70.0) (2017-10-10)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.35.0"></a>
# [1.35.0](https://gitlab.com/4geit/react-packages/compare/v1.34.4...v1.35.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.34.2"></a>
## [1.34.2](https://gitlab.com/4geit/react-packages/compare/v1.34.1...v1.34.2) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.24.1"></a>
## [1.24.1](https://gitlab.com/4geit/react-packages/compare/v1.24.0...v1.24.1) (2017-09-08)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.24.0"></a>
# [1.24.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.24.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.23.0"></a>
# [1.23.0](https://gitlab.com/4geit/react-packages/compare/v1.22.0...v1.23.0) (2017-09-08)


### Features

* **header:** add logout logic ([9689fc9](https://gitlab.com/4geit/react-packages/commit/9689fc9))




<a name="1.22.0"></a>
# [1.22.0](https://gitlab.com/4geit/react-packages/compare/v1.21.1...v1.22.0) (2017-09-08)


### Features

* **notification:** separate notification snackbar into a new component ([e528aba](https://gitlab.com/4geit/react-packages/commit/e528aba))




<a name="1.21.0"></a>
# [1.21.0](https://gitlab.com/4geit/react-packages/compare/v1.20.0...v1.21.0) (2017-09-08)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.17.1"></a>
## [1.17.1](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v1.17.1) (2017-09-07)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.17.0"></a>
# [1.17.0](https://gitlab.com/4geit/react-packages/compare/v1.16.1...v1.17.0) (2017-09-07)


### Bug Fixes

* **login:** added history.replace ([1950051](https://gitlab.com/4geit/react-packages/commit/1950051))
* **login:** code add ([77e2d24](https://gitlab.com/4geit/react-packages/commit/77e2d24))
* **login:** code review ([2b50348](https://gitlab.com/4geit/react-packages/commit/2b50348))
* **login:** props ([213eb33](https://gitlab.com/4geit/react-packages/commit/213eb33))


### Features

* **Login component:** add a redirection ([5bddd00](https://gitlab.com/4geit/react-packages/commit/5bddd00))




<a name="1.12.0"></a>
# [1.12.0](https://gitlab.com/4geit/react-packages/compare/v1.16.0...v1.12.0) (2017-09-07)


### Bug Fixes

* **login:** added history.replace ([1950051](https://gitlab.com/4geit/react-packages/commit/1950051))
* **login:** code add ([77e2d24](https://gitlab.com/4geit/react-packages/commit/77e2d24))
* **login:** code review ([2b50348](https://gitlab.com/4geit/react-packages/commit/2b50348))
* **login:** props ([213eb33](https://gitlab.com/4geit/react-packages/commit/213eb33))


### Features

* **Login component:** add a redirection ([5bddd00](https://gitlab.com/4geit/react-packages/commit/5bddd00))




<a name="1.11.0"></a>
# [1.11.0](https://gitlab.com/4geit/react-packages/compare/v1.9.0...v1.11.0) (2017-09-01)


### Features

* **login/register:** add cardWidth prop ([a80c73f](https://gitlab.com/4geit/react-packages/commit/a80c73f))




<a name="1.10.0"></a>
# [1.10.0](https://gitlab.com/4geit/react-packages/compare/v1.9.0...v1.10.0) (2017-08-31)


### Features

* **login/register:** add cardWidth prop ([a80c73f](https://gitlab.com/4geit/react-packages/commit/a80c73f))




<a name="1.9.0"></a>
# [1.9.0](https://gitlab.com/4geit/react-packages/compare/v1.8.0...v1.9.0) (2017-08-31)


### Features

* **header:** add new component + upgrade MUI and fix breaking changes ([8ac2ee5](https://gitlab.com/4geit/react-packages/commit/8ac2ee5))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.15"></a>
## [1.6.15](https://gitlab.com/4geit/react-packages/compare/v1.6.14...v1.6.15) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.13"></a>
## [1.6.13](https://gitlab.com/4geit/react-packages/compare/v1.6.12...v1.6.13) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.12"></a>
## [1.6.12](https://gitlab.com/4geit/react-packages/compare/v1.6.11...v1.6.12) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/4geit/react-packages/compare/v1.6.8...v1.6.9) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/4geit/react-packages/compare/v1.6.7...v1.6.8) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/4geit/react-packages/compare/v1.6.6...v1.6.7) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/4geit/react-packages/compare/v1.6.5...v1.6.6) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/4geit/react-packages/compare/v1.6.4...v1.6.5) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-login-component

<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/4geit/react-packages/compare/1.6.3...1.6.4) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-login-component
