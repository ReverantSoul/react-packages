import { observable, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import commonStore from '@4geit/rct-common-store'
import notificationStore from '@4geit/rct-notification-store'

class RctAuthStore {
  @observable inProgress = false
  @observable values = {
    email: '',
    password: '',
    firstname: '',
    lastname: '',
    remember: false,
    acknowledge: false,
  }

  @action setEmail(value) {
    this.values.email = value
  }
  @action setPassword(value) {
    this.values.password = value
  }
  @action setRemember(value) {
    this.values.remember = value
  }
  @action setAcknowledge(value) {
    this.values.acknowledge = value
  }
  @action reset() {
    this.values.email = ''
    this.values.password = ''
    this.values.firstname = ''
    this.values.lastname = ''
    this.values.remember = false
    this.values.acknowledge = false
  }
  @action async login() {
    this.inProgress = true
    console.log('Sending form', JSON.stringify(toJS(this.values), null, 2))
    try {
      const { email, password } = this.values
      const { body } = await swaggerClientStore.client.apis.Account.login({
        account: { email, password }
      })
      const { token, firstname, lastname, email: emailField, password: passwordField, company, address, phone } = body
      console.log(body)
      commonStore.setToken(token)
      commonStore.setUser({ firstname, lastname, email: emailField, password: passwordField, company, address, phone })
      await swaggerClientStore.buildClientWithToken({ token })
      runInAction(() => {
        this.inProgress = false
        notificationStore.newMessage('You are logged in!')
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async register() {
    this.inProgress = true
    console.log('Sending form', JSON.stringify(toJS(this.values), null, 2))
    try {
      const { email, password, firstname, lastname, acknowledge } = this.values
      if (!acknowledge) {
        notificationStore.newMessage('You have to agree on the registration terms.')
        this.inProgress = false
        return
      }
      const { body } = await swaggerClientStore.client.apis.Account.register({
        account: { email, password, firstname, lastname }
      })
      console.log(body)
      runInAction(() => {
        this.inProgress = false
        notificationStore.newMessage('You are registered!')
      })
    }
    catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctAuthStore()
