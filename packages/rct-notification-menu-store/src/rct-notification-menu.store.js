// eslint-disable-next-line
import { observable, action, runInAction, toJS } from 'mobx'
// eslint-disable-next-line
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

class RctNotificationMenuStore {
  @observable page = 1
  @observable counter = 5
  @observable data = []
  @observable open = false
  @observable element = undefined
  @observable inProgress = false
  @observable totalCount = 0

  @action resetCounter() {
    this.counter = 0
  }
  @action reset() {
    this.data = []
    this.page = 1
  }
  @action setOpen(value) {
    this.open = value
  }
  @action setElement(value) {
    this.element = value
  }
  @action appendData(value) {
    this.data = this.data || []
    this.data = this.data.concat(value)
    this.page++;
    console.log(this.data)
  }
  @action async fetchData({ operationId }) {
    this.inProgress = true
    try {
      console.log('fetchData')
      const { body, headers: { 'x-totalcount': _totalCount } } = await swaggerClientStore.client.apis.Account[operationId]({
        pageSize: 10,
        page: this.page,
      })
      this.totalCount = parseInt(_totalCount)
      if (body && body.length) {
        runInAction(() => {
          this.appendData(body)
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctNotificationMenuStore()
