# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.76.1"></a>
## [1.76.1](https://gitlab.com/4geit/react-packages/compare/v1.76.0...v1.76.1) (2017-10-20)


### Bug Fixes

* **notification-menu-store:** remove unnecessary mapping ([83b6301](https://gitlab.com/4geit/react-packages/commit/83b6301))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)


### Bug Fixes

* **storybook:** store user info ([4a39bc3](https://gitlab.com/4geit/react-packages/commit/4a39bc3))




<a name="1.69.1"></a>
## [1.69.1](https://gitlab.com/4geit/react-packages/compare/v1.69.0...v1.69.1) (2017-10-10)


### Bug Fixes

* **minor:** minor ([771fc11](https://gitlab.com/4geit/react-packages/commit/771fc11))
* **notification-menu-component:** fix UI issues ([66d5db9](https://gitlab.com/4geit/react-packages/commit/66d5db9))




<a name="1.67.1"></a>
## [1.67.1](https://gitlab.com/4geit/react-packages/compare/v1.67.0...v1.67.1) (2017-10-09)


### Bug Fixes

* **notification-menu-store:** activate fetchData method to use mock api, change avator icon logic ([e712899](https://gitlab.com/4geit/react-packages/commit/e712899))
* **notification-menu-store:** changes to data structure ([cbc9ef5](https://gitlab.com/4geit/react-packages/commit/cbc9ef5))




<a name="1.63.0"></a>
# [1.63.0](https://gitlab.com/4geit/react-packages/compare/v1.62.1...v1.63.0) (2017-10-06)


### Bug Fixes

* **minor:** minor ([9f7a286](https://gitlab.com/4geit/react-packages/commit/9f7a286))


### Features

* **notification-menu-store:** add pagination to load more button and progress circular ([d9984b0](https://gitlab.com/4geit/react-packages/commit/d9984b0))




<a name="1.62.1"></a>
## [1.62.1](https://gitlab.com/4geit/react-packages/compare/v1.62.0...v1.62.1) (2017-10-06)


### Bug Fixes

* **notification-menu-component:** fix horizontal scroll and set id to have unique prop key ([eaedda7](https://gitlab.com/4geit/react-packages/commit/eaedda7))




<a name="1.59.0"></a>
# [1.59.0](https://gitlab.com/4geit/react-packages/compare/v1.58.1...v1.59.0) (2017-10-05)


### Features

* **rct-notification-menu-component:** add badge as well as avatar icon ([7af3075](https://gitlab.com/4geit/react-packages/commit/7af3075))




<a name="1.58.1"></a>
## [1.58.1](https://gitlab.com/4geit/react-packages/compare/v1.58.0...v1.58.1) (2017-10-05)


### Bug Fixes

* **notification-menu-component:** fixed notification content ([b92b0ed](https://gitlab.com/4geit/react-packages/commit/b92b0ed))




<a name="1.52.1"></a>
## [1.52.1](https://gitlab.com/4geit/react-packages/compare/v1.52.0...v1.52.1) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add isRead logic for notification background ([7dc67a8](https://gitlab.com/4geit/react-packages/commit/7dc67a8))




<a name="1.51.1"></a>
## [1.51.1](https://gitlab.com/4geit/react-packages/compare/v1.51.0...v1.51.1) (2017-10-04)


### Bug Fixes

* **notification-menu-store:** add observables and action methods ([e7944f9](https://gitlab.com/4geit/react-packages/commit/e7944f9))
* **notification-menu-store:** move mock data to store, add store to storybook ([f6295b4](https://gitlab.com/4geit/react-packages/commit/f6295b4))




<a name="1.51.0"></a>
# [1.51.0](https://gitlab.com/4geit/react-packages/compare/v1.50.0...v1.51.0) (2017-10-04)


### Features

* **notification-menu-store:** create notification menu store ([7d53464](https://gitlab.com/4geit/react-packages/commit/7d53464))
