import React, { Component } from 'react'
import { Link } from 'react-router-dom'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
// eslint-disable-next-line
import Drawer from 'material-ui/Drawer'
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import Typography from 'material-ui/Typography'
import Divider from 'material-ui/Divider'
import Button from 'material-ui/Button'
import MenuIcon from 'material-ui-icons/Menu'
import IconButton from 'material-ui/IconButton'
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft'

import './rct-right-side-menu.component.css'

const drawerWidth = 600;

@withStyles(theme => ({
  root: {
    [theme.breakpoints.down('lg')]: {
      width: '900px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '1200px',
    },
    margin: 'auto',
    padding: '0 20px',
    height: '100vh',
    zIndex: 1,
    overflow: 'hidden',
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  drawerPaper: {
    position: 'fixed',
    height: '100vh',
    width: drawerWidth,
    marginTop: '68px',
    overflowX: 'hidden',
  },
  drawerPaperClose: {
    width: 0,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  drawerInner: {
    // Make the items inside not wrap when transitioning:
    width: drawerWidth,
  },
  drawerInnerInner: {
    paddingLeft: 20,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    height: 56,
    [theme.breakpoints.up('sm')]: {
      height: 64,
    },
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    height: '100vh',
    [theme.breakpoints.up('sm')]: {
      content: {
        height: '100vh',
      },
    },
  },
  contentShift: {
    marginLeft: 0,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    right: 0,
    position: 'fixed',
    paddingLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  contentComponent: PropTypes.object,
  // TBD
})
@defaultProps({
  // TBD
})
export default class RctRightSideMenuComponent extends Component {
  state = {
    open: true,
  };

  handleDrawerOpen = () => {
    this.setState({open: true});
  };

  handleDrawerClose = () => {
    this.setState({ open: false});
  };

  render() {
    const { classes, children, contentComponent } = this.props
    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <main className={classNames(classes.content, this.state.open && classes.contentShift)}>
            { contentComponent }
          </main>
          <IconButton onClick={this.handleDrawerOpen} className={classNames(classes.menuButton, this.state.open && classes.hide)}>
            <MenuIcon />
          </IconButton>
          <Drawer
            type="persistent"
            anchor="right"
            open={this.state.open}
            classes={{
              paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose)
            }}
          >
            <div className={classes.drawerInner}>
              <div className={classes.drawerHeader}>
                <IconButton onClick={this.handleDrawerClose}>
                  <ChevronLeftIcon />
                </IconButton>
              </div>
              <Divider />
              <div className={classes.drawerInnerInner}>
                { children }
              </div>
            </div>
          </Drawer>
        </div>
      </div>
    )
  }
}
