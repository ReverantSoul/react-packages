# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.75.2"></a>
## [1.75.2](https://gitlab.com/4geit/react-packages/compare/v1.75.1...v1.75.2) (2017-10-17)


### Bug Fixes

* **chatbox-grid:** minor fix ([62f4012](https://gitlab.com/4geit/react-packages/commit/62f4012))




<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.62.0"></a>
# [1.62.0](https://gitlab.com/4geit/react-packages/compare/v1.61.0...v1.62.0) (2017-10-06)


### Bug Fixes

* **ChatboxGrid:** minor fix ([0764768](https://gitlab.com/4geit/react-packages/commit/0764768))


### Features

* **ChatboxGrid:** Fix Style ([9855c50](https://gitlab.com/4geit/react-packages/commit/9855c50))




<a name="1.61.0"></a>
# [1.61.0](https://gitlab.com/4geit/react-packages/compare/v1.60.0...v1.61.0) (2017-10-06)


### Bug Fixes

* **Chatbox Grid:** minor fix ([118605e](https://gitlab.com/4geit/react-packages/commit/118605e))
* **chatboxgrid:** minor fix ([48888bb](https://gitlab.com/4geit/react-packages/commit/48888bb))
* **ChatboxGrid:** Add message logic ([bf606a6](https://gitlab.com/4geit/react-packages/commit/bf606a6))
* minor fix ([3ac52e7](https://gitlab.com/4geit/react-packages/commit/3ac52e7))


### Features

* **Chatbox grid:** Add list message logic ([f7eb2ab](https://gitlab.com/4geit/react-packages/commit/f7eb2ab))




<a name="1.54.0"></a>
# [1.54.0](https://gitlab.com/4geit/react-packages/compare/v1.53.0...v1.54.0) (2017-10-05)


### Bug Fixes

* **Chatbox grid:** minor fix ([94f043d](https://gitlab.com/4geit/react-packages/commit/94f043d))
* **chatboxgrid:** minor fix ([b867903](https://gitlab.com/4geit/react-packages/commit/b867903))
* **ChatboxGrid:** minor fix ([37e52fd](https://gitlab.com/4geit/react-packages/commit/37e52fd))
* **ChatboxGrid:** minor fix ([2ef5c1a](https://gitlab.com/4geit/react-packages/commit/2ef5c1a))
* **ChatboxGrid:** minor fix ([eccb7cc](https://gitlab.com/4geit/react-packages/commit/eccb7cc))
* **ChatboxGrid:** minor fix ([39e7850](https://gitlab.com/4geit/react-packages/commit/39e7850))


### Features

* **Chatbox Grid Store & Chatbox grid component:** Added an action method in the store and trigger e ([9d15062](https://gitlab.com/4geit/react-packages/commit/9d15062))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.47.0"></a>
# [1.47.0](https://gitlab.com/4geit/react-packages/compare/v1.46.0...v1.47.0) (2017-10-03)


### Bug Fixes

* **Chatboxgrid:** minor fix ([aa6c6b2](https://gitlab.com/4geit/react-packages/commit/aa6c6b2))
* **ChatboxGrid:** minor changes ([528fcfc](https://gitlab.com/4geit/react-packages/commit/528fcfc))


### Features

* **Chatbox Grid:** Add Maximize button ([d2a850c](https://gitlab.com/4geit/react-packages/commit/d2a850c))
* **ChatboxGrid:** Add Onclick directive ([27aa892](https://gitlab.com/4geit/react-packages/commit/27aa892))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-component

<a name="1.45.0"></a>
# [1.45.0](https://gitlab.com/4geit/react-packages/compare/v1.44.5...v1.45.0) (2017-10-02)


### Bug Fixes

* **chatboxgrid:** minor fix ([b98724e](https://gitlab.com/4geit/react-packages/commit/b98724e))
* **chatboxgrid:** minor fix ([025d70f](https://gitlab.com/4geit/react-packages/commit/025d70f))
* **chatboxgrid:** minor fix css ([248b9ab](https://gitlab.com/4geit/react-packages/commit/248b9ab))
* **ChatboxGridComponent:** css minor change ([536b7de](https://gitlab.com/4geit/react-packages/commit/536b7de))


### Features

* **Chatboxgrid:** Added border to the style of ListItem ([e1f0c4d](https://gitlab.com/4geit/react-packages/commit/e1f0c4d))
* **ChatboxGrid:** Added Avatar, minor css change ([031b032](https://gitlab.com/4geit/react-packages/commit/031b032))
* **ChatboxGrid:** padding ([484dd71](https://gitlab.com/4geit/react-packages/commit/484dd71))
* **style chatbox:** added css ([5acf043](https://gitlab.com/4geit/react-packages/commit/5acf043))
* **Style itemcomponent:** css ([f6bdbc6](https://gitlab.com/4geit/react-packages/commit/f6bdbc6))




<a name="1.44.3"></a>
## [1.44.3](https://gitlab.com/4geit/react-packages/compare/v1.44.2...v1.44.3) (2017-09-27)


### Bug Fixes

* **chatbox-grid:** refactor operation ID prop types ([aa7029d](https://gitlab.com/4geit/react-packages/commit/aa7029d))
* **reorderable-grid-list:** fix setPosition call with destructured param instead of fixed params ([3e64768](https://gitlab.com/4geit/react-packages/commit/3e64768))




<a name="1.43.1"></a>
## [1.43.1](https://gitlab.com/4geit/react-packages/compare/v1.43.0...v1.43.1) (2017-09-25)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-component

<a name="1.42.0"></a>
# [1.42.0](https://gitlab.com/4geit/react-packages/compare/v1.41.0...v1.42.0) (2017-09-22)


### Features

* **rct-chatbox-grid-component:** start integrating reorderable-grid-list-component ([9e4febd](https://gitlab.com/4geit/react-packages/commit/9e4febd))




<a name="1.36.0"></a>
# [1.36.0](https://gitlab.com/4geit/react-packages/compare/v1.35.0...v1.36.0) (2017-09-20)


### Features

* **Chatbox-grid component and store:** Add add a prop to define the API endpoint to call when the c ([f7f997e](https://gitlab.com/4geit/react-packages/commit/f7f997e))




<a name="1.34.4"></a>
## [1.34.4](https://gitlab.com/4geit/react-packages/compare/v1.34.3...v1.34.4) (2017-09-20)


### Bug Fixes

* **chatbox grid:** minor fix ([0d0fcc1](https://gitlab.com/4geit/react-packages/commit/0d0fcc1))
* **Chatboxgrid store:** add an activate method ([880ddcb](https://gitlab.com/4geit/react-packages/commit/880ddcb))




<a name="1.33.0"></a>
# [1.33.0](https://gitlab.com/4geit/react-packages/compare/v1.32.2...v1.33.0) (2017-09-19)


### Features

* **Store - Add API endpoint:** Added fetchData, addItem and deleteItem methods ([7e0325b](https://gitlab.com/4geit/react-packages/commit/7e0325b))




<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-component

<a name="1.32.1"></a>
## [1.32.1](https://gitlab.com/4geit/react-packages/compare/v1.32.0...v1.32.1) (2017-09-18)


### Bug Fixes

* **store:** minor fix ([4de7a70](https://gitlab.com/4geit/react-packages/commit/4de7a70))




<a name="1.32.0"></a>
# [1.32.0](https://gitlab.com/4geit/react-packages/compare/v1.31.0...v1.32.0) (2017-09-18)


### Features

* **Switch:** added a selection Box in the GridTileBar ([05f8b22](https://gitlab.com/4geit/react-packages/commit/05f8b22))
* **Switch:** work on css ([417af8f](https://gitlab.com/4geit/react-packages/commit/417af8f))




<a name="1.29.3"></a>
## [1.29.3](https://gitlab.com/4geit/react-packages/compare/v1.29.2...v1.29.3) (2017-09-15)


### Bug Fixes

* **chatboxGridComponent:** set cols to 4, add static data, some style changes ([afaaa1e](https://gitlab.com/4geit/react-packages/commit/afaaa1e))




<a name="1.29.2"></a>
## [1.29.2](https://gitlab.com/4geit/react-packages/compare/v1.1.0...v1.29.2) (2017-09-14)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-component

<a name="1.29.1"></a>
## [1.29.1](https://gitlab.com/4geit/react-packages/compare/v1.1.0...v1.29.1) (2017-09-14)




**Note:** Version bump only for package @4geit/rct-chatbox-grid-component

<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/4geit/react-packages/compare/v1.29.0...v1.1.0) (2017-09-14)


### Features

* **Chatbox Component:** Draw a grid list ([5962446](https://gitlab.com/4geit/react-packages/commit/5962446))
