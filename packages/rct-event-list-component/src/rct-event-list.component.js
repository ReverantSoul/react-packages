import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import ListSubheader from 'material-ui/List/ListSubheader'
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List'
import Divider from 'material-ui/Divider'
import Icon from 'material-ui/Icon'
import Typography from 'material-ui/Typography'
import Chip from 'material-ui/Chip'
import Grid from 'material-ui/Grid'
import moment from 'moment-timezone'
import Button from 'material-ui/Button'
import { CircularProgress } from 'material-ui/Progress'

import RctDatePickerComponent from '@4geit/rct-date-picker-component'

import './rct-event-list.component.css'

@withStyles(theme => ({
  root: {
    width: '100%',
  },
  button: {
    height: 50,
  },
  sticky: {
    zIndex: 0,
  }
}))
@withWidth()
@inject('eventListStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  operationId: PropTypes.string.isRequired,
  merchandId: PropTypes.string.isRequired,
  bookingUrl: PropTypes.string.isRequired,
  topTitle: PropTypes.string,
  // TBD
})
@defaultProps({
  operationId: 'timeslots'
})
export default class RctEventListComponent extends Component {
  async componentWillMount() {
    const { eventListStore, operationId } = this.props
    await eventListStore.fetchData({ operationId })
  }
  async handleSearch() {
    const { eventListStore, operationId } = this.props
    eventListStore.reset()
    await eventListStore.fetchData({ operationId })
  }
  async handleSeeMore() {
    const { eventListStore, operationId } = this.props
    await eventListStore.fetchData({ operationId })
  }
  getChip({ startTime, timeZone, events }) {
    if (events.length && events[0].attendees && events[0].attendees >= events[0].maxOcc) {
      return 'Sold out!'
    }
    return moment(startTime).tz(timeZone).format('h:mma z')
  }
  isSoldOut({ events }) {
    if (events.length && events[0].attendees && events[0].attendees >= events[0].maxOcc) {
      return true
    }
    return false
  }
  getTitle({ events, title }) {
    let candidate = title
    if (events.length) {
      candidate = events[0].title
    }
    if (candidate.length > 45) {
      candidate = `${candidate.substring(0, 45)}...`
    }
    return candidate
  }
  render() {
    const { classes, eventListStore, merchandId, bookingUrl, topTitle } = this.props
    const { inProgress, sortedData } = eventListStore
    if (!sortedData.length) {
      return (
        <Typography>Loading events...</Typography>
      )
    }
    return (
      <div className={ classes.root } >
        <br/>
        {/* title */}
        { topTitle && (
          <Typography type="display1">{ topTitle }</Typography>
        ) }
        <br/>
        {/* date picker + search button */}
        <Grid container justify="center">
          <Grid item>
            <RctDatePickerComponent placeholder='Choose date' calendarIcon months={1} />
          </Grid>
          <Grid item>
            <Button color='primary' raised classes={ {
              root: classes.button
            } } onClick={ this.handleSearch.bind(this) } >
              Search
            </Button>
          </Grid>
        </Grid>
        <br/><br/>
        {/* list */}
        { sortedData.map(({ date, timeslots }) => (
          <List key={ date } subheader={
            <ListSubheader classes={ { sticky: classes.sticky } }>
              <Typography type="title">{ moment(date).format('MMMM Do, YYYY') }</Typography>
            </ListSubheader>
          } >
            <br/>
            { timeslots.map(({ eventId, activity, events, title, startTime, timeZone }, index) => {
              const { color } = activity
              const eventInstanceId = `${eventId}_${moment(startTime).tz('UTC').format('YYYYMMDDTHHmmss\\Z')}`
              return (
                <ListItem key={ index } disabled={ this.isSoldOut({ events }) } button component='a' href={ `${bookingUrl}/#/merchant/${merchandId}/activity/${activity._id}/event/${eventInstanceId}` }>
                  <ListItemIcon>
                    <Icon style={ { color: `#${color}` } }>fiber_manual_record</Icon>
                  </ListItemIcon>
                  {/* time + title */}
                  <ListItemText primary={
                    <Grid container alignItems="center">
                      {/* time */}
                      <Grid item>
                        <Chip label={ this.getChip({ startTime, timeZone, events }) } />
                      </Grid>
                      {/* title */}
                      <Grid item>
                        <Typography type="subheading">
                          { this.getTitle({ events, title }) }
                        </Typography>
                      </Grid>
                    </Grid>
                  } />
                </ListItem>
              )
            }) }
            <br/>
          </List>
        )) }
        { /* see more */ }
        <Grid container justify="center">
          <Grid item>
            { inProgress && (
              <CircularProgress/>
            ) }
            { !inProgress && (
              <Button color='primary' classes={ {
                root: classes.button
              } } onClick={ this.handleSeeMore.bind(this) } >
                See More
              </Button>
            ) }
          </Grid>
        </Grid>
        {/* hack */}
        <br/><br/><br/><br/><br/><br/>
      </div>
    )
  }
}
