# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.70.1"></a>
## [1.70.1](https://gitlab.com/4geit/react-packages/compare/v1.70.0...v1.70.1) (2017-10-10)




**Note:** Version bump only for package @4geit/rct-event-list-component

<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.44.5"></a>
## [1.44.5](https://gitlab.com/4geit/react-packages/compare/v1.44.4...v1.44.5) (2017-10-02)


### Bug Fixes

* **event-list:** remove debug ([fc13e22](https://gitlab.com/4geit/react-packages/commit/fc13e22))
* **event-list-store:** add sorting by hour, and limit data to last event startTime ([f62dd7f](https://gitlab.com/4geit/react-packages/commit/f62dd7f))
* **event-list-store:** handle case when events list empty ([3c8e5e8](https://gitlab.com/4geit/react-packages/commit/3c8e5e8))




<a name="1.44.4"></a>
## [1.44.4](https://gitlab.com/4geit/react-packages/compare/v1.44.3...v1.44.4) (2017-09-28)




**Note:** Version bump only for package @4geit/rct-event-list-component

<a name="1.42.0"></a>
# [1.42.0](https://gitlab.com/4geit/react-packages/compare/v1.41.0...v1.42.0) (2017-09-22)


### Bug Fixes

* **merge:** merge master ([36e1634](https://gitlab.com/4geit/react-packages/commit/36e1634))


### Features

* **reorderable-grid-list-component:** add a new reorderable grid list component + refactor componen ([09fa0da](https://gitlab.com/4geit/react-packages/commit/09fa0da))




<a name="1.40.1"></a>
## [1.40.1](https://gitlab.com/4geit/react-packages/compare/v1.40.0...v1.40.1) (2017-09-22)


### Bug Fixes

* **event-list:** use events title + truncate + disable soldout events ([8dd6ce9](https://gitlab.com/4geit/react-packages/commit/8dd6ce9))




<a name="1.40.0"></a>
# [1.40.0](https://gitlab.com/4geit/react-packages/compare/v1.39.4...v1.40.0) (2017-09-22)


### Features

* **event-list-component:** add pagination feature ([dd38138](https://gitlab.com/4geit/react-packages/commit/dd38138))




<a name="1.39.4"></a>
## [1.39.4](https://gitlab.com/4geit/react-packages/compare/v1.39.3...v1.39.4) (2017-09-21)




**Note:** Version bump only for package @4geit/rct-event-list-component

<a name="1.39.3"></a>
## [1.39.3](https://gitlab.com/4geit/react-packages/compare/v1.39.2...v1.39.3) (2017-09-21)


### Bug Fixes

* **event-list-store:** use correct parameters of timeslots to filter with date-picker ([a0a8f42](https://gitlab.com/4geit/react-packages/commit/a0a8f42))




<a name="1.39.2"></a>
## [1.39.2](https://gitlab.com/4geit/react-packages/compare/v1.39.1...v1.39.2) (2017-09-21)


### Bug Fixes

* **date-picker-component:** fix issue with observable assignment ([5ddf8f4](https://gitlab.com/4geit/react-packages/commit/5ddf8f4))




<a name="1.39.1"></a>
## [1.39.1](https://gitlab.com/4geit/react-packages/compare/v1.39.0...v1.39.1) (2017-09-21)


### Bug Fixes

* **eventListComponent:** fix months prop ([e7809d1](https://gitlab.com/4geit/react-packages/commit/e7809d1))




<a name="1.39.0"></a>
# [1.39.0](https://gitlab.com/4geit/react-packages/compare/v1.38.0...v1.39.0) (2017-09-21)


### Bug Fixes

* **date-picker-component:** remove button and add it to eventListComponent ([d474352](https://gitlab.com/4geit/react-packages/commit/d474352))
* **minor:** minor ([0e2f966](https://gitlab.com/4geit/react-packages/commit/0e2f966))




<a name="1.38.0"></a>
# [1.38.0](https://gitlab.com/4geit/react-packages/compare/v1.37.0...v1.38.0) (2017-09-21)


### Features

* **event-list-component:** add event time as a chip + add dotenv support to storybook ([294ed14](https://gitlab.com/4geit/react-packages/commit/294ed14))




<a name="1.37.0"></a>
# [1.37.0](https://gitlab.com/4geit/react-packages/compare/v1.36.0...v1.37.0) (2017-09-20)


### Features

* **event-list-component:** improved UI component + integrate new API structure ([e7e0805](https://gitlab.com/4geit/react-packages/commit/e7e0805))




<a name="1.34.3"></a>
## [1.34.3](https://gitlab.com/4geit/react-packages/compare/v1.34.2...v1.34.3) (2017-09-20)


### Bug Fixes

* **event-list-component:** add default value to operationId ([fd17f20](https://gitlab.com/4geit/react-packages/commit/fd17f20))
* **event-list-store:** create fechData action method, add componentWillMount method in eventListComp ([970691e](https://gitlab.com/4geit/react-packages/commit/970691e))
* **minor:** minor ([c21935a](https://gitlab.com/4geit/react-packages/commit/c21935a))




<a name="1.34.1"></a>
## [1.34.1](https://gitlab.com/4geit/react-packages/compare/v1.34.0...v1.34.1) (2017-09-20)


### Bug Fixes

* **package.json:** add dependency to eventListStore ([4c35fcf](https://gitlab.com/4geit/react-packages/commit/4c35fcf))




<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)


### Bug Fixes

* **eventListComponent:** transform sortedData into a list and iterate over it to display the list of ([ee7f4f9](https://gitlab.com/4geit/react-packages/commit/ee7f4f9))
* **package.json:** remove dependency to eventListStore ([0dbed29](https://gitlab.com/4geit/react-packages/commit/0dbed29))


### Features

* **eventListComponent:** add eventlistComponent and Store as well as function to group events by da ([0e6ec1c](https://gitlab.com/4geit/react-packages/commit/0e6ec1c))
