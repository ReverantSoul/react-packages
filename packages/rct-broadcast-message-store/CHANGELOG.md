# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.70.1"></a>
## [1.70.1](https://gitlab.com/4geit/react-packages/compare/v1.70.0...v1.70.1) (2017-10-10)




**Note:** Version bump only for package @4geit/rct-broadcast-message-store

<a name="1.48.1"></a>
## [1.48.1](https://gitlab.com/4geit/react-packages/compare/v1.48.0...v1.48.1) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-broadcast-message-store

<a name="1.47.1"></a>
## [1.47.1](https://gitlab.com/4geit/react-packages/compare/v1.47.0...v1.47.1) (2017-10-03)


### Bug Fixes

* **broadcast-message:** use new addOperationId ([b6b704b](https://gitlab.com/4geit/react-packages/commit/b6b704b))




<a name="1.43.0"></a>
# [1.43.0](https://gitlab.com/4geit/react-packages/compare/v1.42.0...v1.43.0) (2017-09-24)


### Bug Fixes

* **broadcast-message-store:** changes to API call ([34f2dc6](https://gitlab.com/4geit/react-packages/commit/34f2dc6))
* **broadcast-message-store:** fix async method ([9b956ab](https://gitlab.com/4geit/react-packages/commit/9b956ab))


### Features

* **broadcast-message-store:** add broadcast-message-store and props to define API endpoint ([269d68f](https://gitlab.com/4geit/react-packages/commit/269d68f))
