import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import List, { ListItem, ListItemIcon, ListItemText, ListItemSecondaryAction, ListSubheader } from 'material-ui/List'
import ChatIcon from 'material-ui-icons/Chat'
import Switch from 'material-ui/Switch'

import './rct-chatbox-list.component.css'

@withStyles(theme => ({
  root: {
    // [theme.breakpoints.down('md')]: {
    //   width: '100%',
    // },
    width: 300,
  },
  // TBD
}))
@withWidth()
@inject('chatboxListStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  listOperationId: PropTypes.string.isRequired,
  addOperationId: PropTypes.string.isRequired,
  deleteOperationId: PropTypes.string.isRequired,
  // TBD
})
@defaultProps({
  listOperationId: 'chatboxStatusList',
  addOperationId: 'userChatboxAdd',
  deleteOperationId: 'userChatboxDelete',
  // TBD
})
export default class RctChatboxListComponent extends Component {
  constructor() {
    super()
    this.handleSwitchChange = ({ id }) => (event, checked) => {
      const { chatboxListStore, addOperationId, deleteOperationId } = this.props
      if (checked) {
        chatboxListStore.addUserChatbox({ addOperationId, id })
      } else {
        chatboxListStore.removeUserChatbox({ deleteOperationId, id })
      }
    }
  }

  async componentWillMount() {
    const { listOperationId } = this.props
    await this.props.chatboxListStore.fetchData({ listOperationId })
  }

  render() {
    const { classes } = this.props
    const { chatboxList } = this.props.chatboxListStore
    return (
      <List className={ classes.root } subheader={ <ListSubheader>Chatboxes</ListSubheader> } >
        { chatboxList.map(({ chatbox: { id, name, description}, status}) => (
          <ListItem button key={ id }>
            <ListItemIcon>
              <ChatIcon/>
            </ListItemIcon>
            <ListItemText primary={ name } secondary={ description } />
            <ListItemSecondaryAction>
              <Switch
                checked={ status }
                onChange={ this.handleSwitchChange({ id }) }/>
            </ListItemSecondaryAction>
          </ListItem>
        )) }
      </List>
    )
  }
}
