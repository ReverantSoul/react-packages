import { observable, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

class RctChatboxListStore {
  @observable inProgress = false
  @observable chatboxList = []

  @action setChatboxList(value) {
    this.chatboxList = value
  }
  @action async fetchData({ listOperationId }) {
    listOperationId = listOperationId || 'chatboxStatusList'
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[listOperationId]()
      if (body.length) {
        runInAction(() => {
          this.setChatboxList(body)
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async addUserChatbox({ addOperationId, id }) {
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[addOperationId]({
        body: {
          chatboxId: id,
        }
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async removeUserChatbox({ deleteOperationId, id }) {
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[deleteOperationId]({ id })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctChatboxListStore()
