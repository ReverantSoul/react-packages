import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
// eslint-disable-next-line
import classNames from 'classnames'
import IconButton from 'material-ui/IconButton'
import PersonIcon from 'material-ui-icons/PersonPin'
import DateIcon from 'material-ui-icons/DateRange'
import AccountIcon from 'material-ui-icons/AccountBalance'
import PaymentIcon from 'material-ui-icons/Payment'
import CardIcon from 'material-ui-icons/CardTravel'
import AssignmentIcon from 'material-ui-icons/Assignment'
import RemoveIcon from 'material-ui-icons/RemoveCircleOutline'
import Menu, { MenuItem } from 'material-ui/Menu'
import Avatar from 'material-ui/Avatar'
import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import Divider from 'material-ui/Divider'
import Button from 'material-ui/Button'
import Badge from 'material-ui/Badge'
import { CircularProgress } from 'material-ui/Progress'
import moment from 'moment'

import './rct-notification-menu.component.css'
import logo from './logo.png.js'

@withStyles(theme => ({
  wrap: {
    whiteSpace: 'normal',
  },
  listStyle: {
    width: '100%!important',
    paddingRight: '0px!important',
  },
  headerMenuItem: {
    background: 'white',
    paddingBottom: '20px'
  },
  client: {
    textDecoration: 'none',
    color: '#2196f3'
  }
}))
@withWidth()
@inject('notificationMenuStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  operationId: PropTypes.string,
  notificationPagePath: PropTypes.string,
})
@defaultProps({
  operationId: 'notificationList'
})

export default class RctNotificationMenuComponent extends Component {
  async componentWillMount() {
    const { notificationMenuStore, operationId } = this.props
    await notificationMenuStore.fetchData({ operationId })
  }
  async handleLoadMore() {
    const { notificationMenuStore, operationId } = this.props
    await notificationMenuStore.fetchData({ operationId })
  }
  handleClick(event) {
    this.props.notificationMenuStore.setOpen(true)
    this.props.notificationMenuStore.setElement(event.currentTarget)
    this.props.notificationMenuStore.resetCounter()
  }
  handleRequestClose() {
    this.props.notificationMenuStore.setOpen(false)
  }
  render() {
    const { classes } = this.props
    const { notificationMenuStore, notificationPagePath } = this.props
    const typePhrases = {
      booking: 'booked',
      adjustment: 'booking adjusted for',
      refund: 'refunded for',
      'partially refunded': 'partially refunded for',
      reservation: 'reserved',
    }
    const typeIcons = {
      booking: <DateIcon/>,
      adjustement: <AssignmentIcon/>,
      refund: <AccountIcon/>,
      'partially refunded': <PaymentIcon/>,
      reservation: <CardIcon/>,
      event: <PersonIcon/>,
      cancellation: <RemoveIcon/>,
    }
    const { inProgress } = notificationMenuStore
    return (
      <div>
        { this.props.notificationMenuStore.counter > 0 && (
          <Badge badgeContent={this.props.notificationMenuStore.counter} color="accent">
            <Avatar src={ logo }
              onClick={this.handleClick.bind(this)}
            />
          </Badge>
        )}
        { this.props.notificationMenuStore.counter === 0 && (
          <Avatar src={ logo }
            onClick={this.handleClick.bind(this)}
          />
        )}
        <Menu
          PaperProps={{
            style: {
              width: 600,
            }
          }}
          className="menu"
          id='long-menu'
          anchorEl={notificationMenuStore.element}
          open={notificationMenuStore.open} onRequestClose={this.handleRequestClose.bind(this)}
        >
          <MenuItem
            className={ classes.headerMenuItem }
            divider='true'
          >
            <Grid container justify='space-between' alignItems='center'>
              <Grid item>
                <Typography type='title'>
                  Notifications
                </Typography>
              </Grid>
              <Grid item>
                <Button color='primary' component='a' href={ notificationPagePath }>
                  See all
                </Button>
              </Grid>
            </Grid>
          </MenuItem>
          {notificationMenuStore.data.map(({ id, activity, event, payment, refund, isClicked, isRead, link, timestamp, client, type, subtype }) => (
            <div key={ id }>
              <MenuItem
                onClick={this.handleRequestClose.bind(this)}
                style={{ background: !isClicked ? '#e6e6e6' : 'none' , height: 100 }}
                divider='true'
              >
                <Grid container alignItems='center'>
                  <Grid item>
                    <Avatar>
                      { type in typeIcons &&  typeIcons[type] }
                    </Avatar>
                  </Grid>
                  <Grid item xs>
                    <Grid container direction='column'>
                      <Grid item>
                        <Typography
                          classes={ {
                            root: classes.wrap
                          } }
                        >
                          { type === 'event' && (
                            <span>Guide, &nbsp;</span>
                          )}
                          { type !== 'cancellation' && (
                            <a href='#' className={ classes.client }>{ client } &nbsp;</a>
                          )}
                          { subtype && type === 'event' && (
                            <span>was { subtype } to &nbsp;</span>
                          )}
                          { type in typePhrases && (
                          <span>{ typePhrases[type] } &nbsp;</span>
                          )}
                          { activity }
                          &nbsp;
                          <span>on &nbsp;</span>
                          { moment(event).format('MMMM Do YYYY, h:mm a') }
                          { type === 'cancellation' && (
                            <span>&nbsp; cancelled</span>
                          )}
                          <span>. &nbsp;</span>
                          { payment && payment.length > 0 && type != 'reservation' && (
                            <span style={{ color: 'green' }}>{ payment }</span>
                          )}
                          { payment && payment.length > 0 && type === 'reservation' && (
                            <span style={{ color: 'yellow' }}>{ payment }</span>
                          )}
                          { !payment && refund && refund.length > 0 && (
                            <span style={{ color: 'red' }}>{ refund }</span>
                          )}
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Grid container justify='flex-end' alignItems='center' spacing={40}>
                          <Grid item>
                            <Button
                              component='a'
                              href={ link }
                              color='primary'
                            >
                              View { type }
                            </Button>
                          </Grid>
                          <Grid item>
                            <Typography type='caption'>
                              { moment(timestamp).format('MM/DD/YYYY, h:mm a') }
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </MenuItem>
            </div>
          ))}
          <MenuItem
            style={{ paddingTop: '20px'}}
          >
            <Grid container justify='center'>
              <Grid item>
                { inProgress && (
                  <CircularProgress/>
                ) }
                { !inProgress && notificationMenuStore.data.length === notificationMenuStore.totalCount && (
                  <Typography type='caption'>All notifications shown</Typography>
                )}
                { !inProgress && notificationMenuStore.data.length < notificationMenuStore.totalCount && (
                  <Button color='primary' onClick={ this.handleLoadMore.bind(this) }>Load more</Button>
                )}
              </Grid>
            </Grid>
          </MenuItem>
        </Menu>
      </div>
    )
  }
}
