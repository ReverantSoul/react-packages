# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.76.1"></a>
## [1.76.1](https://gitlab.com/4geit/react-packages/compare/v1.76.0...v1.76.1) (2017-10-20)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)




**Note:** Version bump only for package @4geit/rct-notification-menu-component

<a name="1.70.2"></a>
## [1.70.2](https://gitlab.com/4geit/react-packages/compare/v1.70.1...v1.70.2) (2017-10-11)


### Bug Fixes

* **package.json:** add moment dependency ([999ccdf](https://gitlab.com/4geit/react-packages/commit/999ccdf))




<a name="1.69.1"></a>
## [1.69.1](https://gitlab.com/4geit/react-packages/compare/v1.69.0...v1.69.1) (2017-10-10)


### Bug Fixes

* **minor:** minor ([6088816](https://gitlab.com/4geit/react-packages/commit/6088816))
* **minor:** minor ([771fc11](https://gitlab.com/4geit/react-packages/commit/771fc11))
* **notification-menu-component:** fix UI issues ([66d5db9](https://gitlab.com/4geit/react-packages/commit/66d5db9))




<a name="1.67.1"></a>
## [1.67.1](https://gitlab.com/4geit/react-packages/compare/v1.67.0...v1.67.1) (2017-10-09)


### Bug Fixes

* **minor:** minor ([ff96e16](https://gitlab.com/4geit/react-packages/commit/ff96e16))
* **notification-menu-store:** activate fetchData method to use mock api, change avator icon logic ([e712899](https://gitlab.com/4geit/react-packages/commit/e712899))
* **notification-menu-store:** changes to data structure ([cbc9ef5](https://gitlab.com/4geit/react-packages/commit/cbc9ef5))




<a name="1.63.1"></a>
## [1.63.1](https://gitlab.com/4geit/react-packages/compare/v1.63.0...v1.63.1) (2017-10-06)


### Bug Fixes

* **minor:** minor ([313f3bf](https://gitlab.com/4geit/react-packages/commit/313f3bf))
* **notification-menu-component:** add path prop to see all button ([bed769d](https://gitlab.com/4geit/react-packages/commit/bed769d))




<a name="1.63.0"></a>
# [1.63.0](https://gitlab.com/4geit/react-packages/compare/v1.62.1...v1.63.0) (2017-10-06)


### Features

* **notification-menu-store:** add pagination to load more button and progress circular ([d9984b0](https://gitlab.com/4geit/react-packages/commit/d9984b0))




<a name="1.62.1"></a>
## [1.62.1](https://gitlab.com/4geit/react-packages/compare/v1.62.0...v1.62.1) (2017-10-06)


### Bug Fixes

* **notification-menu-component:** fix horizontal scroll and set id to have unique prop key ([eaedda7](https://gitlab.com/4geit/react-packages/commit/eaedda7))




<a name="1.59.0"></a>
# [1.59.0](https://gitlab.com/4geit/react-packages/compare/v1.58.1...v1.59.0) (2017-10-05)


### Features

* **rct-notification-menu-component:** add badge as well as avatar icon ([7af3075](https://gitlab.com/4geit/react-packages/commit/7af3075))




<a name="1.58.1"></a>
## [1.58.1](https://gitlab.com/4geit/react-packages/compare/v1.58.0...v1.58.1) (2017-10-05)


### Bug Fixes

* **minor:** minor ([f7b9fa0](https://gitlab.com/4geit/react-packages/commit/f7b9fa0))
* **notification-menu-component:** fixed notification content ([b92b0ed](https://gitlab.com/4geit/react-packages/commit/b92b0ed))




<a name="1.52.1"></a>
## [1.52.1](https://gitlab.com/4geit/react-packages/compare/v1.52.0...v1.52.1) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add isRead logic for notification background ([7dc67a8](https://gitlab.com/4geit/react-packages/commit/7dc67a8))
* **notification-menu-component:** fix anchorEl issue ([74dd0e1](https://gitlab.com/4geit/react-packages/commit/74dd0e1))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.51.2"></a>
## [1.51.2](https://gitlab.com/4geit/react-packages/compare/v1.51.1...v1.51.2) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** fix binding issue ([57b8bab](https://gitlab.com/4geit/react-packages/commit/57b8bab))




<a name="1.51.1"></a>
## [1.51.1](https://gitlab.com/4geit/react-packages/compare/v1.51.0...v1.51.1) (2017-10-04)


### Bug Fixes

* **notification-menu-store:** add observables and action methods ([e7944f9](https://gitlab.com/4geit/react-packages/commit/e7944f9))
* **notification-menu-store:** move mock data to store, add store to storybook ([f6295b4](https://gitlab.com/4geit/react-packages/commit/f6295b4))




<a name="1.50.0"></a>
# [1.50.0](https://gitlab.com/4geit/react-packages/compare/v1.49.0...v1.50.0) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add prop to define operationId to call endpoint ([be80f7e](https://gitlab.com/4geit/react-packages/commit/be80f7e))




<a name="1.48.2"></a>
## [1.48.2](https://gitlab.com/4geit/react-packages/compare/v1.48.1...v1.48.2) (2017-10-03)


### Bug Fixes

* **notification-menu-component:** add notification button, menu component and content ([731bcc6](https://gitlab.com/4geit/react-packages/commit/731bcc6))
* **notification-menu-component:** minor ([66fed9e](https://gitlab.com/4geit/react-packages/commit/66fed9e))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)


### Features

* **notification-menu-component:** create notification menu componenet and added it to storybook ([73b44e2](https://gitlab.com/4geit/react-packages/commit/73b44e2))
