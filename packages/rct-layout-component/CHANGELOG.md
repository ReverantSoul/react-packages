# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.74.1"></a>
## [1.74.1](https://gitlab.com/4geit/react-packages/compare/v1.74.0...v1.74.1) (2017-10-13)


### Bug Fixes

* **right-side-menu-component:** fix error in sidemenu and content ([579064e](https://gitlab.com/4geit/react-packages/commit/579064e))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-layout-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-layout-component

<a name="1.29.0"></a>
# [1.29.0](https://gitlab.com/4geit/react-packages/compare/v1.28.0...v1.29.0) (2017-09-14)


### Bug Fixes

* **layout-component:** apply responsive class to children prop in case there is a RightSideMenuCompo ([7bec304](https://gitlab.com/4geit/react-packages/commit/7bec304))
* **layout-component:** fix issue with rightSideMenuComponent no showing and duplication ([9fbecdd](https://gitlab.com/4geit/react-packages/commit/9fbecdd))
* **rightSideMenuComponent:** wrap _rightSideMenuComponent into an if clause to handle case where rig ([d0b447a](https://gitlab.com/4geit/react-packages/commit/d0b447a))




<a name="1.22.0"></a>
# [1.22.0](https://gitlab.com/4geit/react-packages/compare/v1.21.1...v1.22.0) (2017-09-08)


### Features

* **notification:** separate notification snackbar into a new component ([e528aba](https://gitlab.com/4geit/react-packages/commit/e528aba))




<a name="1.17.1"></a>
## [1.17.1](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v1.17.1) (2017-09-07)




**Note:** Version bump only for package @4geit/rct-layout-component

<a name="1.17.0"></a>
# [1.17.0](https://gitlab.com/4geit/react-packages/compare/v1.16.1...v1.17.0) (2017-09-07)


### Bug Fixes

* **layout:** fix sidemenu display in layout component, remove overflow, fix toolbar elements display ([539f856](https://gitlab.com/4geit/react-packages/commit/539f856))
* **layout:** remove duplicate sidemenu component call ([c0cd1be](https://gitlab.com/4geit/react-packages/commit/c0cd1be))
* **layout:** remove toolbar from header, add header content to sidemenu component and finally use it ([12ca4a6](https://gitlab.com/4geit/react-packages/commit/12ca4a6))
* **layout-component:** minor changes ([99126b7](https://gitlab.com/4geit/react-packages/commit/99126b7))




<a name="1.16.0"></a>
# [1.16.0](https://gitlab.com/4geit/react-packages/compare/v1.16.1...v1.16.0) (2017-09-07)


### Bug Fixes

* **layout:** fix sidemenu display in layout component, remove overflow, fix toolbar elements display ([539f856](https://gitlab.com/4geit/react-packages/commit/539f856))
* **layout:** remove duplicate sidemenu component call ([c0cd1be](https://gitlab.com/4geit/react-packages/commit/c0cd1be))
* **layout:** remove toolbar from header, add header content to sidemenu component and finally use it ([12ca4a6](https://gitlab.com/4geit/react-packages/commit/12ca4a6))
* **layout-component:** minor changes ([99126b7](https://gitlab.com/4geit/react-packages/commit/99126b7))




<a name="1.11.0"></a>
# [1.11.0](https://gitlab.com/4geit/react-packages/compare/v1.9.0...v1.11.0) (2017-09-01)


### Features

* **header:** add search bar ([fad09f0](https://gitlab.com/4geit/react-packages/commit/fad09f0))




<a name="1.9.0"></a>
# [1.9.0](https://gitlab.com/4geit/react-packages/compare/v1.8.0...v1.9.0) (2017-08-31)


### Features

* **header:** add new component + upgrade MUI and fix breaking changes ([8ac2ee5](https://gitlab.com/4geit/react-packages/commit/8ac2ee5))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-layout-component
