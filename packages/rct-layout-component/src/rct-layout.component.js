import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'

import compose from 'recompose/compose'
import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Grid from 'material-ui/Grid'
import AppBar from 'material-ui/AppBar'
import Paper from 'material-ui/Paper'
import Typography from 'material-ui/Typography'

import RctNotificationComponent from '@4geit/rct-notification-component'

import './rct-layout.component.css'

@withStyles(theme => ({
  fill: {
    height: '100%',
  },
  responsive: {
    [theme.breakpoints.down('lg')]: {
      width: '900px',
    },
    [theme.breakpoints.up('lg')]: {
      width: '1200px',
    },
    margin: 'auto',
    padding: '0 20px',
  },
  header: {
    minHeight: '50px',
  },
  middleTop: {
    paddingTop: '90px',
    backgroundColor: '#fff',
  },
  middleBottom: {
    padding: '10px',
    backgroundColor: '#fff',
    borderTop: '1px solid #eee',
  },
  footer: {
    borderTop: '1px solid #eee',
    backgroundColor: '#fff',
    height: '50px',
  },
  sideMenu: {
  },
  content: {
    borderLeft: '1px solid #eee',
    padding: '10px',
  },
}))
@withWidth()
// @inject('xyzStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  topComponent: PropTypes.object.isRequired,
  sideMenuComponent: PropTypes.object.isRequired,
  rightSideMenuComponent: PropTypes.object,
  footerComponent: PropTypes.object,
  notificationComponent: PropTypes.object.isRequired,
})
@defaultProps({
  notificationComponent: <RctNotificationComponent/>
})
export default class RctLayoutComponent extends Component {
  render() {
    const { children, classes, topComponent, sideMenuComponent, middleBottomComponent, footerComponent, notificationComponent, rightSideMenuComponent } = this.props
    let _rightSideMenuComponent
    if(rightSideMenuComponent) {
      _rightSideMenuComponent = React.cloneElement(rightSideMenuComponent, { contentComponent: children })
    }
    const _sideMenuComponent = React.cloneElement(sideMenuComponent, { topComponent, contentComponent:  _rightSideMenuComponent ? _rightSideMenuComponent : children })

    return (
      <div className={ classes.fill } >
        {/* root */}
        <div className={ classes.fill } >
          {/* middle */}
          <div>
            {/* side menu */}
            { _sideMenuComponent && (
              <div className={ classes.sideMenu } >
                { _sideMenuComponent }
              </div>
            ) }
            {/* !side menu */}
          </div>
          {/* !middle */}
          {/* footer */}
          { footerComponent && (
            <div className={ classNames(classes.responsive, classes.footer) } >
              { footerComponent }
            </div>
          ) }
          {/* !footer */}
          {/* notification */}
          { notificationComponent }
          {/* !notification */}
        </div>
        {/* !root */}
      </div>
    )
  }
}
