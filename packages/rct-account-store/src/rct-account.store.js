// eslint-disable-next-line
import { observable, action, runInAction, toJS } from 'mobx'
// eslint-disable-next-line
import { hashHistory } from 'react-router'


class RctAccountStore {
  @observable body

  @action setBody(value) {
    this.body = value
  }
  @action setAddressField(value, key) {
    this.body.address[key] = value
  }
  @action setBodyField(key, value) {
    this.body[key] = value
  }

  @action async userUpdate({ updateOperationId, itemId }) {
    updateOperationId = updateOperationId || 'userUpdate'
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[updateOperationId]({
        id: itemId,
        body: this.body
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}
export default new RctAccountStore()
