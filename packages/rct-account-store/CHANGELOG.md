# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.76.0"></a>
# [1.76.0](https://gitlab.com/4geit/react-packages/compare/v1.75.2...v1.76.0) (2017-10-19)


### Bug Fixes

* **account store:** handle on change ([200b3ce](https://gitlab.com/4geit/react-packages/commit/200b3ce))
* **account store:** minor fix ([1a24fbc](https://gitlab.com/4geit/react-packages/commit/1a24fbc))
* **account store:** minor fix ([8c83937](https://gitlab.com/4geit/react-packages/commit/8c83937))


### Features

* **Account store:** Add Update method ([c175cfc](https://gitlab.com/4geit/react-packages/commit/c175cfc))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)


### Features

* **Account Store:** create fetchData method to get user info ([461c460](https://gitlab.com/4geit/react-packages/commit/461c460))




<a name="1.74.0"></a>
# [1.74.0](https://gitlab.com/4geit/react-packages/compare/v1.73.0...v1.74.0) (2017-10-12)


### Features

* **AccountBox:** Add user textfields ([67fb588](https://gitlab.com/4geit/react-packages/commit/67fb588))




<a name="1.73.0"></a>
# [1.73.0](https://gitlab.com/4geit/react-packages/compare/v1.72.0...v1.73.0) (2017-10-12)


### Features

* **account store:** create package account store ([44f1e7c](https://gitlab.com/4geit/react-packages/commit/44f1e7c))
