# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.76.0"></a>
# [1.76.0](https://gitlab.com/4geit/react-packages/compare/v1.75.2...v1.76.0) (2017-10-19)


### Bug Fixes

* **account store:** handle on change ([200b3ce](https://gitlab.com/4geit/react-packages/commit/200b3ce))
* **account store:** minor fix ([8c83937](https://gitlab.com/4geit/react-packages/commit/8c83937))


### Features

* **Account Component:** add handleOnChange method to update account fields on changes ([37d531b](https://gitlab.com/4geit/react-packages/commit/37d531b))
* **Account store:** Add Update method ([c175cfc](https://gitlab.com/4geit/react-packages/commit/c175cfc))




<a name="1.75.0"></a>
# [1.75.0](https://gitlab.com/4geit/react-packages/compare/v1.74.1...v1.75.0) (2017-10-16)


### Bug Fixes

* **account:** minor fix ([6bc9ea3](https://gitlab.com/4geit/react-packages/commit/6bc9ea3))
* **account component:** minor fix ([2fc342c](https://gitlab.com/4geit/react-packages/commit/2fc342c))
* **account component:** minor fix ([03e2809](https://gitlab.com/4geit/react-packages/commit/03e2809))
* **account component:** minor fix ([bbdcf35](https://gitlab.com/4geit/react-packages/commit/bbdcf35))
* **account store:** minor fix ([0ba9547](https://gitlab.com/4geit/react-packages/commit/0ba9547))
* **storybook:** store user info ([4a39bc3](https://gitlab.com/4geit/react-packages/commit/4a39bc3))


### Features

* **Account Store:** create fetchData method to get user info ([461c460](https://gitlab.com/4geit/react-packages/commit/461c460))




<a name="1.74.0"></a>
# [1.74.0](https://gitlab.com/4geit/react-packages/compare/v1.73.0...v1.74.0) (2017-10-12)


### Bug Fixes

* **account component:** minor fix ([3ac2ad9](https://gitlab.com/4geit/react-packages/commit/3ac2ad9))


### Features

* **AccountBox:** Add user textfields ([67fb588](https://gitlab.com/4geit/react-packages/commit/67fb588))




<a name="1.72.0"></a>
# [1.72.0](https://gitlab.com/4geit/react-packages/compare/v1.71.0...v1.72.0) (2017-10-12)


### Features

* **account componenet:** create component ([aaeed01](https://gitlab.com/4geit/react-packages/commit/aaeed01))
