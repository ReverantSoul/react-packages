import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
import { observable, action, toJS } from 'mobx'
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import classNames from 'classnames'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import Button from 'material-ui/Button'
import Typography from 'material-ui/Typography'
import TextField from 'material-ui/TextField'
import Grid from 'material-ui/Grid'
import AccountBoxIcon from 'material-ui-icons/AccountBox'
import { FormControlLabel } from 'material-ui/Form'
import './rct-account.component.css'

@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@inject('accountStore', 'commonStore')
@observer
@propTypes({
  cardWidth: PropTypes.string,
})
@defaultProps({
})
export default class RctAccountComponent extends Component {
  handleAccountChange(event) {
    this.props.accountStore.setBodyField(event.target.name, event.target.value)
  }
  handleUpdate = () => {
    const { updateOperationId, itemId, accountStore } = this.props
    accountStore.userUpdate({ updateOperationId, itemId, })
  }
  render() {
    const { cardWidth } = this.props
    const { user: { firstname, lastname, email, password, company, phone, address: { street, city, state, postcode, country } } } = this.props.commonStore
    return (
      <div style={{
        width: cardWidth ? cardWidth : '100%',
      }} >
        <Card>
          <CardHeader
            style={{
              backgroundColor:'#2196f3',
              color: 'white'
            }}
            title="My Account"
            avatar={ <AccountBoxIcon/> }
          />
          <CardContent>
            <Typography type="body1" gutterBottom>
              Fill out the fields above with your information.
            </Typography>
            <div>
              <TextField
                label="Email"
                type="email"
                id="email"
                name="email"
                value={email}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
            </div>
            <div>
              <TextField
                label="Password"
                type="password"
                id="password"
                name="password"
                value={password}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
              />
            </div>
            <div>
              <TextField
                label="Firstname"
                type="firstname"
                id="firstname"
                name="firstname"
                value={firstname}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
            </div>
            <div>
              <TextField
                label="Lastname"
                type="lastname"
                id="lastname"
                name="lastname"
                value={lastname}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
            </div>
            <div>
              <TextField
                label="Company"
                type="company"
                id="company"
                name="company"
                value={company}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
            </div>
            <div>
              <TextField
                label="Street"
                type="street"
                id="street"
                name="street"
                value={street}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
              <TextField
                label="State"
                type="state"
                id="state"
                name="state"
                value={state}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
              <TextField
                label="Postcode"
                type="postcode"
                id="postcode"
                name="postcode"
                value={postcode}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
              <TextField
                label="Country"
                type="country"
                id="country"
                name="country"
                value={country}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
            </div>
            <div>
              <TextField
                label="Phone"
                type="phone"
                id="phone"
                name="phone"
                value={phone}
                onChange={ this.handleAccountChange.bind(this) }
                fullWidth={true}
                required={true}
                autoFocus={true}
              />
            </div>
          </CardContent>
          <CardActions>
            <Grid container justify="center">
              <Grid item>
                <Button
                  raised
                  color="primary"
                  type="submit"
                >Update</Button>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      </div>
    )
  }
}
