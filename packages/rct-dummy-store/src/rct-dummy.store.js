import { observable, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

class RctDummyStore {
  // @observable var1
  // @observable var2
  // @action setVar1(value) {
  //   this.var1 = value
  // }
  // @action setVar2(value) {
  //   this.var2 = value
  // }
  // @computed get dynamicVar1() {
  //   return this.var1.filter(i => ...)
  // }
}

export default new RctDummyStore()
export { RctDummyStore }
