# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.70.1"></a>
## [1.70.1](https://gitlab.com/4geit/react-packages/compare/v1.70.0...v1.70.1) (2017-10-10)




**Note:** Version bump only for package @4geit/rct-broadcast-message-component

<a name="1.56.0"></a>
# [1.56.0](https://gitlab.com/4geit/react-packages/compare/v1.55.0...v1.56.0) (2017-10-05)


### Features

* **broadcast-message:** add placeholder to show the maximized chatbox name ([cc1b553](https://gitlab.com/4geit/react-packages/commit/cc1b553))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.48.2"></a>
## [1.48.2](https://gitlab.com/4geit/react-packages/compare/v1.48.1...v1.48.2) (2017-10-03)


### Bug Fixes

* **broadcast-message:** use new endpoint operation id ([6cbff47](https://gitlab.com/4geit/react-packages/commit/6cbff47))




<a name="1.48.1"></a>
## [1.48.1](https://gitlab.com/4geit/react-packages/compare/v1.48.0...v1.48.1) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-broadcast-message-component

<a name="1.47.1"></a>
## [1.47.1](https://gitlab.com/4geit/react-packages/compare/v1.47.0...v1.47.1) (2017-10-03)


### Bug Fixes

* **broadcast-message:** use new addOperationId ([b6b704b](https://gitlab.com/4geit/react-packages/commit/b6b704b))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-broadcast-message-component

<a name="1.44.2"></a>
## [1.44.2](https://gitlab.com/4geit/react-packages/compare/v1.44.1...v1.44.2) (2017-09-26)


### Bug Fixes

* **broadcast-message-component:** fix issue with xs prop in grid ([8a8c9c3](https://gitlab.com/4geit/react-packages/commit/8a8c9c3))




<a name="1.43.0"></a>
# [1.43.0](https://gitlab.com/4geit/react-packages/compare/v1.42.0...v1.43.0) (2017-09-24)


### Bug Fixes

* **broadcast-message-store:** changes to API call ([34f2dc6](https://gitlab.com/4geit/react-packages/commit/34f2dc6))
* **broadcast-message-store:** fix async method ([9b956ab](https://gitlab.com/4geit/react-packages/commit/9b956ab))
* **minor:** minor ([31bb7f7](https://gitlab.com/4geit/react-packages/commit/31bb7f7))


### Features

* **broadcast-message-store:** add broadcast-message-store and props to define API endpoint ([269d68f](https://gitlab.com/4geit/react-packages/commit/269d68f))




<a name="1.41.0"></a>
# [1.41.0](https://gitlab.com/4geit/react-packages/compare/v1.40.1...v1.41.0) (2017-09-22)


### Bug Fixes

* **broadcast-message-component:** changed layout and style, add props for label and text helper ([5a68457](https://gitlab.com/4geit/react-packages/commit/5a68457))




<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-broadcast-message-component

<a name="1.31.0"></a>
# [1.31.0](https://gitlab.com/4geit/react-packages/compare/v1.30.0...v1.31.0) (2017-09-17)


### Bug Fixes

* **RctBroadcastMessageComponent:** add the textfield and buttons to the broadcastMessageComponent ([9516a93](https://gitlab.com/4geit/react-packages/commit/9516a93))


### Features

* **RctBroadcastMessageComponent:** create broadcastMessageComponent ([e36f64c](https://gitlab.com/4geit/react-packages/commit/e36f64c))
