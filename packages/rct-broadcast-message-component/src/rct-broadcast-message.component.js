import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import TextField from 'material-ui/TextField'
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'

import './rct-broadcast-message.component.css'

@withStyles(theme => ({
  form: {
    width: '100%'
  }
}))
@withWidth()
@inject('broadcastMessageStore', 'chatboxGridStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  helper: PropTypes.string,
  label: PropTypes.string.isRequired,
  addOperationId: PropTypes.string.isRequired,
  listOperationId: PropTypes.string.isRequired,
})
@defaultProps({
  listOperationId: 'userChatboxList',
  addOperationId: 'messageBulkAdd',
})
export default class RctBroadcastMessageComponent extends Component {
  async componentWillMount() {
    const { chatboxGridStore, listOperationId } = this.props
    await chatboxGridStore.fetchMaximizedItem({ listOperationId })
  }
  componentWillUnmount() {
    this.props.broadcastMessageStore.reset()
  }
  handleMessageChange(event) {
    this.props.broadcastMessageStore.setMessage(event.target.value)
  }
  async handleSubmitForm(event) {
    const { broadcastMessageStore, addOperationId } = this.props
    await broadcastMessageStore.send({ addOperationId })
  }
  render() {
    const { classes, helper, label, broadcastMessageStore, chatboxGridStore } = this.props
    const { message } = broadcastMessageStore
    const { maximizedItem } = chatboxGridStore
    return (
      <div>
        <Card>
          <CardContent>
            <Grid container alignItems='center' justify='space-between'>
              <Grid item xs={11}>
                <form className={ classes.form } onSubmit={ this.handleSubmitForm.bind(this) }>
                  <TextField
                    label={ label }
                    helperText={ maximizedItem ? `Send message to ${maximizedItem.chatbox.name}` : helper }
                    fullWidth
                    margin="normal"
                    value={ message }
                    onChange={ this.handleMessageChange.bind(this) }
                  />
                </form>
              </Grid>
              <Grid item xs={1}>
                <Grid container justify='center'>
                  <Grid item>
                    <Button dense raised color="primary" type="submit">Send</Button>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    )
  }
}
