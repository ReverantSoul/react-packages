import React, { Component } from 'react'
import { Link } from 'react-router-dom'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'

import AppBar from 'material-ui/AppBar'
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card'
import Toolbar from 'material-ui/Toolbar'
import Typography from 'material-ui/Typography'
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton'
import AccountBoxIcon from 'material-ui-icons/AccountBox'
import PowerSettingsNewIcon from 'material-ui-icons/PowerSettingsNew'
import compose from 'recompose/compose'
import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Input from 'material-ui/Input/Input'
import Grid from 'material-ui/Grid'
import { withRouter } from 'react-router-dom';

import RctSearchInputComponent from '@4geit/rct-search-input-component'

import './rct-header.component.css'

@withStyles(theme => ({
  flex: {
    flex: 1,
  },
  button: {
    margin: theme.spacing.unit,
  },
}))
@withWidth()
@inject('commonStore', 'notificationStore')
@withRouter
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  logo: PropTypes.any.isRequired,
  accountRoute: PropTypes.string.isRequired,
  logoutRedirectTo: PropTypes.string.isRequired,
})
@defaultProps({
  accountRoute: '/account',
  loginRoute: '/login',
  logoutRedirectTo: '/login',
})
export default class RctHeaderComponent extends Component {
  handleLogout() {
    this.props.commonStore.logout()
    this.props.notificationStore.newMessage('You are logged out!')
    this.props.history.replace(this.props.logoutRedirectTo)
  }
  render() {
    const { classes, logo, accountRoute, logoutRoute, loginRoute } = this.props
    const { isLoggedIn, firstname, lastname } = this.props.commonStore
    return (
      <Grid container alignItems="center"
        style = {{
          marginRight: 'auto'
        }}>
        {/* logo */}
        <Grid item xs>
          <Link to="/">
            <img src={ logo } alt="Logo" style={{
              height: '50px',
            }} />
          </Link>
        </Grid>
        {/* !logo */}
        {/* searchbar */}
        <Grid item>
          <RctSearchInputComponent/>
        </Grid>
        {/* !searchbar */}
        {/* actions */}
        <Grid item>
          { !isLoggedIn && (
            <IconButton component={ Link } to={ loginRoute } color="contrast">
              <AccountBoxIcon/>
            </IconButton>
          ) }
          { isLoggedIn && (
            <IconButton component={ Link } to={ accountRoute } color="contrast">
              <AccountBoxIcon/>
            </IconButton>
          ) }
          { isLoggedIn && (
            <IconButton aria-label="Logout" color="contrast" onClick={ this.handleLogout.bind(this) }>
              <PowerSettingsNewIcon/>
            </IconButton>
          ) }
        </Grid>
        <Grid item>
          <Typography  type="body1"
            style={{
              color: 'inherit',
            }}>
            { firstname } { lastname }
          </Typography>
        </Grid>
        {/* !actions */}
      </Grid>
    )
  }
}
