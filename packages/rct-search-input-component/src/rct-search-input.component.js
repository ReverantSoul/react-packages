import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'

import Input from 'material-ui/Input/Input'
import compose from 'recompose/compose'
import classNames from 'classnames'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import IconButton from 'material-ui/IconButton'
import SearchIcon from 'material-ui-icons/Search'
import Grid from 'material-ui/Grid'

import './rct-search-input.component.css'

@withStyles(theme => ({
  root: {
    color: 'white',
  },
  focused: {
    width: '250px',
  },
  inkbar: {
    backgroundColor: 'white',
  },
  input: {
    '&:before': {
      backgroundColor: 'white',
    },
  },
}))
@withWidth()
// @inject('xyzStore')
@observer
@propTypes({
  // TBD
})
@defaultProps({
  // TBD
})
export default class RctSearchInputComponent extends Component {
  render() {
    const { classes } = this.props
    return (
      <Grid container alignItems="center" style={{
        height: '30px'
      }}>
        <Grid item>
          <Input
            placeholder="Search"
            classes={{
              root: classes.root,
              focused: classes.focused,
            }}
            className={ classes.input }
            inputProps={{
              'aria-label': 'Description',
            }}
          />
        </Grid>
        <Grid item>
          <IconButton color="contrast">
            <SearchIcon/>
          </IconButton>
        </Grid>
      </Grid>
    )
  }
}
