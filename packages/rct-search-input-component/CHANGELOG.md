# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.75.1"></a>
## [1.75.1](https://gitlab.com/4geit/react-packages/compare/v1.75.0...v1.75.1) (2017-10-16)


### Bug Fixes

* **upgrade:** upgrade to last version of MUI ([91a14ab](https://gitlab.com/4geit/react-packages/commit/91a14ab))




<a name="1.52.0"></a>
# [1.52.0](https://gitlab.com/4geit/react-packages/compare/v1.51.2...v1.52.0) (2017-10-04)


### Features

* **chatbox-grid:** add fetchMaximizedItem to store and use within component ([e1df4e4](https://gitlab.com/4geit/react-packages/commit/e1df4e4))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-search-input-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-search-input-component

<a name="1.12.0"></a>
# [1.12.0](https://gitlab.com/4geit/react-packages/compare/v1.11.0...v1.12.0) (2017-09-02)


### Features

* **header:** add search bar ([c799a59](https://gitlab.com/4geit/react-packages/commit/c799a59))




<a name="1.11.0"></a>
# [1.11.0](https://gitlab.com/4geit/react-packages/compare/v1.9.0...v1.11.0) (2017-09-01)


### Features

* **scripts:** add styling code base to components ([e797fcc](https://gitlab.com/4geit/react-packages/commit/e797fcc))
* **search-input:** add new component + update README ([d12181a](https://gitlab.com/4geit/react-packages/commit/d12181a))
