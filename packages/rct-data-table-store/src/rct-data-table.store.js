import { observable, computed, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

export class Column {
  @observable name
  @observable enabled = false
  @observable type = 'string'

  constructor({ name, enabled, type }) {
    this.name = name
    this.enabled = enabled
    this.type = type
  }

  @action setEnabled(value) {
    this.enabled = value
  }
}

class RctDataTableStore {
  @observable inProgress = false
  @observable columnsMenuOpen = false
  @observable columnsMenuElement = undefined
  @observable addDialogOpen = false
  @observable editDialogOpen = false
  @observable importDialogOpen = false
  @observable addDialogFields = {}
  @observable editDialogFields = {}
  @observable editDialogItem
  @observable removeSnackbarOpen = false
  @observable removeId = undefined
  @observable removeAllSnackbarOpen = false
  @observable removeAllIds = []
  @observable columns = []
  @observable data = []
  @observable selected = []
  @observable importedData = []

  @action setColumnsMenuOpen(value) {
    this.columnsMenuOpen = value
  }
  @action setColumnsMenuElement(value) {
    this.columnsMenuElement = value
  }
  @action setAddDialogOpen(value) {
    this.addDialogOpen = value
  }
  @action setAddDialogField(name, value) {
    this.addDialogFields[name] = value
  }
  @action setEditDialogOpen(value) {
    this.editDialogOpen = value
  }
  @action setEditDialogField(name, value) {
    this.editDialogFields[name] = value
  }
  @action setEditDialogItem(value) {
    this.editDialogItem = value
  }
  @action setImportDialogOpen(value) {
    this.importDialogOpen = value
  }
  @action setRemoveSnackbarOpen(value) {
    this.removeSnackbarOpen = value
  }
  @action setRemoveId(value) {
    this.removeId = value
  }
  @action setRemoveAllSnackbarOpen(value) {
    this.removeAllSnackbarOpen = value
  }
  @action setRemoveAllIds(value) {
    this.removeAllIds = value
  }
  @action setSelected(value) {
    this.selected = value
  }
  @action setImportedData(value) {
    this.importedData = value
  }
  @action reset() {
    this.columnsMenuOpen = false
    this.columnsMenuElement = undefined
    this.addDialogOpen = false
    this.editDialogOpen = false
  }
  @computed get enabledColumns() {
    return this.columns.filter(i => i.enabled)
  }
  @computed get mutableColumns() {
    return this.columns.filter(i => !(['_id', 'id', 'user'].includes(i.name)))
  }
  @action setColumns(value) {
    this.columns = value.map(({ name, enabled, type }) => new Column({ name, enabled, type }))
  }
  @action setData(value) {
    this.data = value
  }
  @action async fetchData({ listOperationId, enabledColumns }) {
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[listOperationId]()
      if (body.length) {
        runInAction(() => {
          this.setColumns(
            Object.entries( body[0] ).map(([ key, value ]) => ( {
              name: key,
              enabled: !!~enabledColumns.indexOf(key),
              type: typeof value,
            } ))
          )
          this.setData( body )
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async removeItem({ deleteOperationId, id }) {
    deleteOperationId = deleteOperationId || 'productDelete'
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      const { body } = await Account[deleteOperationId]({
        id,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async addItem({ addOperationId, body }) {
    addOperationId = addOperationId || 'productAdd'
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[addOperationId]({
        body,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async importItem({ bulkAddOperationId, body }) {
    bulkAddOperationId = bulkAddOperationId || 'productBulkAdd'
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[bulkAddOperationId]({
        body,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async editItem({ updateOperationId, id, body }) {
    updateOperationId = updateOperationId || 'productUpdate'
    this.inProgress = true
    try {
      const { client: { apis: { Account } } } = swaggerClientStore
      await Account[updateOperationId]({
        id,
        body,
      })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctDataTableStore()
